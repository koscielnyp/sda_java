package main;

import model.Komputer;
import model.Garnitur;
import model.Sklep;
import model.Kolor;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Sklep sklep = new Sklep();
        Scanner scanner = new Scanner(System.in);
        //System.out.println(new Komputer(1, "fs", 1, 2));
        String wybor;
        String nowaCena;
        String nowaNazwa;
        String nowyRam;
        String noweCpu;
        String nowyRozmiar;
        Kolor nowyKolor = null;

        do {
            System.out.println("1 - dodaj nowy komputer");
            System.out.println("2 - dodaj nowy garnitur");
            System.out.println("3 - wyswietl liste wszystkich produkt�w");
            System.out.println("4 - usun komputer");
            System.out.println("5 - usun garnitur");
            System.out.println("");

            System.out.println("q - zakoncz program");

            System.out.print("Twoj wybor: ");
            wybor = scanner.next();

            switch (wybor) {
                case "1":
                	System.out.println("Podaj cene:");
                	nowaCena = scanner.next();
                	System.out.println("Podaj nazwe:");
                	nowaNazwa = scanner.next();
                	System.out.println("Podaj ilosc RAMu:");
                	nowyRam = scanner.next();
                	System.out.println("Podaj nazwe CPU:");
                	noweCpu = scanner.next();
                	if (sklep.czyJestMiejsceNaKomputery()) {
                		Komputer nowyKomputer = new Komputer(Double.parseDouble(nowaCena), nowaNazwa, Double.parseDouble(nowyRam), Double.parseDouble(noweCpu));
                		sklep.dodajKomputer(nowyKomputer);
                	}
                    break;
                case "2":
                	System.out.println("Podaj cene:");
                	nowaCena = scanner.next();
                	System.out.println("Podaj nazwe:");
                	nowaNazwa = scanner.next();
                	System.out.println("Podaj rozmiar:");
                	nowyRozmiar = scanner.next();
                	System.out.println("Podaj kolor: (Dostepne: Czarny, szary, granatowy");
                	do {
                        try {
                        	nowyKolor = Kolor.valueOf(scanner.next().toUpperCase());
                        } catch (IllegalArgumentException e) {
                            System.out.println("Zly kolor, podaj jeszcze raz");
                        }
                	} while (nowyKolor == null);
                	
                	if (sklep.czyJestMiejsceNaGarnitury()) {
                		Garnitur nowyGarnitur = new Garnitur (Double.parseDouble(nowaCena), nowaNazwa, Double.parseDouble(nowyRozmiar), nowyKolor);
                		sklep.dodajGarnitur(nowyGarnitur);
                	}
                    break;
                case "3":
                	sklep.wypiszAsortyment();
                    break;
                case "4":
                	if (sklep.czySaWOgoleKomputery()) {
                    	System.out.println("Podaj ID komputera:");
                    	sklep.usunKomputer(scanner.nextInt());               		
                	}
                    break;
                case "5":
                	if (sklep.czySaWOgoleGarnitury()) {
                    	System.out.println("Podaj ID garnituru:");
                    	sklep.usunGarnitur(scanner.nextInt());               		
                	}
                    break;               	
                case "q":
                    System.out.println("Koniec programu");
                    break;
                default:
                    System.out.println("Niepoprawna opcja");
                    break;

            }
        } while (!wybor.equals("q"));
    }
}
