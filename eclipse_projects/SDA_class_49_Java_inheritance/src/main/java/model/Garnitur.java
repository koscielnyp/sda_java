package model;

public class Garnitur extends Produkt{
    private double rozmiar;
    private Kolor kolor;

    public Garnitur(double cena, String nazwa, double rozmiar, Kolor kolor) {
    	super(cena, nazwa);
        this.rozmiar = rozmiar;
        this.kolor = kolor;
    }

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public double getRozmiar() {
		return rozmiar;
	}

	public void setRozmiar(double rozmiar) {
		this.rozmiar = rozmiar;
	}

	public Kolor getKolor() {
		return kolor;
	}

	public void setKolor(Kolor kolor) {
		this.kolor = kolor;
	}
}
