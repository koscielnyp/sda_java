package model;

public class Komputer extends Produkt {
    private double ram;
    private double cpu;

    public Komputer(double cena, String nazwa, double ram, double cpu) {
    	super(cena, nazwa);
        this.ram = ram;
        this.cpu = cpu;
    }

	public double getRam() {
		return ram;
	}

	public void setRam(double ram) {
		this.ram = ram;
	}

	public double getCpu() {
		return cpu;
	}

	public void setCpu(double cpu) {
		this.cpu = cpu;
	}
    
	@Override
	public String toString() {
				
		return "Cena: "
				+ this.getCena()
				+ ", Nazwa: "
				+ this.getNazwa()
				+ ", Ilosc RAMu: "
				+ this.getRam()
				+ ", Predkosc CPU: "
				+ this.getCpu();
	}
}
