package model;

public class Sklep {

    private static final int ILOSC_KOMPUTEROW = 10;
    private static final int ILOSC_GARNITUROW = 10;

    private Komputer[] komputery;
    private Garnitur[] garnitury;
    
    private int currentKomputer = 0;
    private int currentGarnitur = 0;

    public Sklep() {
        komputery = new Komputer[ILOSC_KOMPUTEROW];
        garnitury = new Garnitur[ILOSC_GARNITUROW];
    }
    
    public boolean czyJestMiejsceNaKomputery() {
    	if (currentKomputer == (ILOSC_KOMPUTEROW - 1)) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public boolean czyJestMiejsceNaGarnitury() {
    	if (currentGarnitur == (ILOSC_GARNITUROW - 1)) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public boolean czySaWOgoleKomputery() {
    	if (currentKomputer == 0) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public boolean czySaWOgoleGarnitury() {
    	if (currentGarnitur == 0) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public void dodajKomputer(Komputer nowyKomputer) {
    	
	    komputery[currentKomputer] = nowyKomputer;
	    currentKomputer++;
    	
    }
    
    public void usunKomputer(int remIdx) {
    	
    	remIdx = remIdx - 1;
    	
    	komputery[remIdx] = null;
    	
    	for (int idx = 0; idx < (currentKomputer - 1); idx++) {
    		if (komputery[idx] == null) {
    			komputery[idx] = komputery[idx + 1];
    			komputery[idx + 1] = null;
    		}
    	}
    	
    	currentKomputer--;
    	
    }
    
    public void usunGarnitur(int remIdx) {
    	
    	remIdx = remIdx - 1;
    	
    	garnitury[remIdx] = null;
    	
    	for (int idx = 0; idx < (currentGarnitur - 1); idx++) {
    		if (garnitury[idx] == null) {
    			garnitury[idx] = garnitury[idx + 1];
    			garnitury[idx + 1] = null;
    		}
    	}
    	
    	currentGarnitur--;    	
    }
    
    public void dodajGarnitur(Garnitur nowyGarnitur) {
    	
    	garnitury[currentGarnitur] = nowyGarnitur;
    	currentGarnitur++;
    	
    }
    
    public void wypiszAsortyment() {
    	
    	if (czySaWOgoleKomputery()) {
    		
    		System.out.println("Komputery:");
    		
        	for (int idx = 0; idx < currentKomputer; idx++) {
        		System.out.println((idx + 1) + ". Cena: "
        							+ komputery[idx].getCena()
        							+ ", Nazwa: "
        							+ komputery[idx].getNazwa()
        							+ ", Ilosc RAMu: "
        							+ komputery[idx].getRam()
        							+ ", Predkosc CPU: "
        							+ komputery[idx].getCpu());
        		
        	}
    	}
    	
    	if (czySaWOgoleGarnitury()) {
    		
        	System.out.println("Garnitury:");
        	for (int idx = 0; idx < currentGarnitur; idx++) {
        		System.out.println((idx + 1) + ". Cena: "
        							+ garnitury[idx].getCena()
        							+ ", Nazwa: "
        							+ garnitury[idx].getNazwa()
        							+ ", Rozmiar: "
        							+ garnitury[idx].getRozmiar()
        							+ ", Kolor: "
        							+ garnitury[idx].getKolor());
        	}
        	
    	}
    	
    }
    
}
