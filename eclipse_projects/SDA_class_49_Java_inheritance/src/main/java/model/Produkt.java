package model;

public class Produkt {

	protected double cena;
	protected String nazwa;
	
	public Produkt(double cena, String nazwa) {
		super();
		this.cena = cena;
		this.nazwa = nazwa;
		
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	
}
