package model;

public class SklepProdukt {

    private static final int ILOSC_PRODUKTOW = 20;

    private Produkt[] produkty;
    
    private int currentProdukt = 0;

    public SklepProdukt() {
    	produkty = new Produkt[ILOSC_PRODUKTOW];
    }
    
    public boolean czyJestMiejsceNaProdukty() {
    	if (currentProdukt == (ILOSC_PRODUKTOW - 1)) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    
    public boolean czySaWOgoleProdukty() {
    	if (currentProdukt == 0) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public void dodajProdukt(Produkt nowyProdukt) {
    	
    	produkty[currentProdukt] = nowyProdukt;
	    currentProdukt++;
    	
    }
    
    public void usunProdukt(int remIdx) {
    	
    	remIdx = remIdx - 1;
    	
    	produkty[remIdx] = null;
    	
    	for (int idx = 0; idx < (currentProdukt - 1); idx++) {
    		if (produkty[idx] == null) {
    			produkty[idx] = produkty[idx + 1];
    			produkty[idx + 1] = null;
    		}
    	}
    	
    	currentProdukt--;
    	
    }
    
    public void wypiszAsortyment() {
    	
    	if (czySaWOgoleProdukty()) {
    		
        	for (int idx = 0; idx < currentProdukt; idx++) {
        		System.out.println((idx + 1) + produkty[idx].toString());	
        	}
    	}
    	
    }
    
}
