package dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import config.HibernateConfig;
import dao.UzytkownikDao;
import model.Uzytkownik;

public class UzytkownikDaoImpl implements UzytkownikDao {

//    private Database database;
//
//    public UzytkownikDaoImpl() {
//        this.database = new Database();
//    }


    public void zapisz(Uzytkownik uzytkownik) {
    	
    	Session session = HibernateConfig.getSession();	
    	session.save(uzytkownik);
    	session.close();
    	
//    	  OLD JDBC METHOD
//        Connection connection = database.openConnection();
//        String sql = "INSERT INTO uzytkownik(imie, nazwisko) VALUES (?,?)";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//
//        statement.setString(1, uzytkownik.getImie());
//        statement.setString(2, uzytkownik.getNazwisko());
//
//        statement.executeUpdate();
//        database.closeConnection();
    }

    public Uzytkownik znajdz(int id) {
    	
    	Uzytkownik uzytkownik = null;
    	
    	Session session = HibernateConfig.getSession();	
    	
    	uzytkownik = session.get(Uzytkownik.class, id);
    	session.close();
    	
    	return uzytkownik;
    	
//  	  OLD JDBC METHOD    	
//        Uzytkownik uzytkownik = null;
//        Connection connection = database.openConnection();
//        String sql = "SELECT id,imie,nazwisko FROM uzytkownik WHERE id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setInt(1, id);
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            String imie = resultSet.getString("imie");
//            String nazwisko = resultSet.getString("nazwisko");
//            uzytkownik = new Uzytkownik(id, imie, nazwisko);
//        }
//        return uzytkownik;
    }

    @SuppressWarnings("unchecked")
	public List<Uzytkownik> znajdzWszystkich() {
    	
    	Session session = HibernateConfig.getSession();	
    	
    	Query query = session.createQuery("from Uzytkownik");
    	List<Uzytkownik> uzytkownicy = query.list();

    	session.close();
    	
    	return uzytkownicy;
    	
//        List<Uzytkownik> uzytkownicy = new ArrayList<>();
//        Connection connection = database.openConnection();
//        String sql = "SELECT id,imie,nazwisko FROM uzytkownik";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            int id = resultSet.getInt("id");
//            String imie = resultSet.getString("imie");
//            String nazwisko = resultSet.getString("nazwisko");
//            Uzytkownik uzytkownik = new Uzytkownik(id, imie, nazwisko);
//            uzytkownicy.add(uzytkownik);
//        }
//        return uzytkownicy;
    }

    public void usun(int id) {
    	
    	Session session = HibernateConfig.getSession();	
    	
    	session.beginTransaction();
    	Uzytkownik uzytkownik = znajdz(id);
    	session.delete(uzytkownik);
    	session.getTransaction().commit();

    	session.close();    	
    	
//        Connection connection = database.openConnection();
//        String sql = "DELETE FROM uzytkownik WHERE id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setInt(1, id);
//        statement.executeUpdate();
    }
}
