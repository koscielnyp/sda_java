package dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import config.HibernateConfig;
import dao.WypozyczenieDao;
import model.Wypozyczenie;

public class WypozyczenieDaoImpl implements WypozyczenieDao {

//    private Database database;
//
//    public WypozyczenieDaoImpl() {
//        this.database = new Database();
//    }

    @Override
    public void zapisz(Wypozyczenie wypozyczenie) {
    	
    	Session session = HibernateConfig.getSession();	
    	session.save(wypozyczenie);
    	session.close();
    	
//        Connection connection = database.openConnection();
//        String sql = "INSERT INTO wypozyczenia(data_wypozyczenia, data_zwrotu, zwrocone, ksiazka_id, uzytkownik_id) VALUES (?,?,?,?,?)";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//
//        statement.setDate(1, new Date(wypozyczenie.getDataWypozyczenia().getTime()));
//        statement.setDate(2, new Date(wypozyczenie.getDataZwrotu().getTime()));
//        statement.setBoolean(3, wypozyczenie.getZwrocone());
//        statement.setInt(4, wypozyczenie.getKsiazka().getId());
//        statement.setInt(5, wypozyczenie.getUzytkownik().getId());
//
//        statement.executeUpdate();
//        database.closeConnection();
    }

    @Override
    public Wypozyczenie znajdz(int id) {
    	
    	Wypozyczenie wypozyczenie = null;
    	
    	Session session = HibernateConfig.getSession();	
    	wypozyczenie = session.get(Wypozyczenie.class, id);
    	session.close();
    	
    	return wypozyczenie;
    	
//        Wypozyczenie wypozyczenie = null;
//        Connection connection = database.openConnection();
//        String sql = "SELECT wypozyczenia.id as wypozyczenie_id, data_wypozyczenia, data_zwrotu, zwrocone, uzytkownik_id, ksiazka_id, imie, nazwisko, tytul, autor FROM wypozyczenia " +
//                "inner join uzytkownik " +
//                "on uzytkownik.id = wypozyczenia.uzytkownik_id " +
//                "inner join ksiazka " +
//                "on ksiazka.id = wypozyczenia.ksiazka_id WHERE wypozyczenia.id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setInt(1, id);
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            int idUzytkownika = resultSet.getInt("uzytkownik_id");
//            String imie = resultSet.getString("imie");
//            String nazwisko = resultSet.getString("nazwisko");
//            int idKsiazki = resultSet.getInt("ksiazka_id");
//            String tytul = resultSet.getString("tytul");
//            String autor = resultSet.getString("autor");
//            Date dataWypozyczenia = resultSet.getDate("data_wypozyczenia");
//            Date dataZwrotu = resultSet.getDate("data_zwrotu");
//            boolean zwrocone = resultSet.getBoolean("zwrocone");
//
//
//            Uzytkownik uzytkownik = new Uzytkownik(idUzytkownika, imie, nazwisko);
//            Ksiazka ksiazka = new Ksiazka(idKsiazki, tytul, autor);
//            wypozyczenie = new Wypozyczenie(id, new java.util.Date(dataWypozyczenia.getTime()),
//                    new java.util.Date(dataZwrotu.getTime()),
//                    zwrocone,
//                    uzytkownik,
//                    ksiazka);
//        }
//        return wypozyczenie;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<Wypozyczenie> znajdzWszystkie() {
    	
    	Session session = HibernateConfig.getSession();	
    	
    	Query query = session.createQuery("from Wypozyczenie");
    	List<Wypozyczenie> wypozyczenia = query.list();

    	session.close();
    	
    	return wypozyczenia;
    	
//        List<Wypozyczenie> wypozyczenia = new ArrayList<>();
//        Connection connection = database.openConnection();
//        String sql = "SELECT wypozyczenia.id as wypozyczenie_id, data_wypozyczenia, data_zwrotu, zwrocone, uzytkownik_id, ksiazka_id, imie, nazwisko, tytul, autor FROM wypozyczenia " +
//                "inner join uzytkownik " +
//                "on uzytkownik.id = wypozyczenia.uzytkownik_id " +
//                "inner join ksiazka " +
//                "on ksiazka.id = wypozyczenia.ksiazka_id";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            int idUzytkownika = resultSet.getInt("uzytkownik_id");
//            String imie = resultSet.getString("imie");
//            String nazwisko = resultSet.getString("nazwisko");
//            int idKsiazki = resultSet.getInt("ksiazka_id");
//            String tytul = resultSet.getString("tytul");
//            String autor = resultSet.getString("autor");
//            int idWypozyczenia = resultSet.getInt("wypozyczenie_id");
//            Date dataWypozyczenia = resultSet.getDate("data_wypozyczenia");
//            Date dataZwrotu = resultSet.getDate("data_zwrotu");
//            boolean zwrocone = resultSet.getBoolean("zwrocone");
//
//
//            Uzytkownik uzytkownik = new Uzytkownik(idUzytkownika, imie, nazwisko);
//            Ksiazka ksiazka = new Ksiazka(idKsiazki, tytul, autor);
//            Wypozyczenie wypozyczenie = new Wypozyczenie(idWypozyczenia,
//                    new java.util.Date(dataWypozyczenia.getTime()),
//                    new java.util.Date(dataZwrotu.getTime()),
//                    zwrocone,
//                    uzytkownik,
//                    ksiazka);
//            wypozyczenia.add(wypozyczenie);
//        }
//        return wypozyczenia;
    }

    @Override
    public void usun(int id) {
    	
    	Session session = HibernateConfig.getSession();	
    	
    	session.beginTransaction();
    	Wypozyczenie wypozyczenie = znajdz(id);
    	session.delete(wypozyczenie);
    	session.getTransaction().commit();

    	session.close();    
    	
//        Connection connection = database.openConnection();
//        String sql = "DELETE FROM wypozyczenia WHERE id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setInt(1, id);
//        statement.executeUpdate();
    }

    @Override
    public void zaktualizuj(Wypozyczenie wypozyczenie) {
    	
    	Session session = HibernateConfig.getSession();	
    	session.update(wypozyczenie);
    	session.close();
    	
//        Connection connection = database.openConnection();
//        String sql = "UPDATE wypozyczenia SET data_zwrotu=?,zwrocone=? WHERE id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//
//        statement.setDate(1, new Date(wypozyczenie.getDataZwrotu().getTime()));
//        statement.setBoolean(2, wypozyczenie.getZwrocone());
//        statement.setInt(3, wypozyczenie.getId());
//
//        statement.executeUpdate();
//        database.closeConnection();
    }
}
