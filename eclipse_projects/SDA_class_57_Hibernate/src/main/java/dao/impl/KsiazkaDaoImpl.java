package dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import config.HibernateConfig;
import dao.KsiazkaDao;
import model.Ksiazka;

public class KsiazkaDaoImpl implements KsiazkaDao {
	
//    private Database database;
//
//    public KsiazkaDaoImpl() {
//        this.database = new Database();
//    }

    @Override
    public void zapisz(Ksiazka ksiazka) {
    	
    	Session session = HibernateConfig.getSession();	
    	session.save(ksiazka);
    	session.close();
    	
//        Connection connection = database.openConnection();
//        String sql = "INSERT INTO ksiazka(tytul, autor) VALUES (?,?)";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//
//        statement.setString(1, ksiazka.getTytul());
//        statement.setString(2, ksiazka.getAutor());
//
//        statement.executeUpdate();
//        database.closeConnection();
    }

    @Override
    public Ksiazka znajdz(int id) {
    	
    	Ksiazka ksiazka = null;
    	
    	Session session = HibernateConfig.getSession();	
    	
    	ksiazka = session.get(Ksiazka.class, id);
    	session.close();
    	
    	return ksiazka;
    	
//        Ksiazka ksiazka = null;
//        Connection connection = database.openConnection();
//        String sql = "SELECT id,tytul,autor FROM ksiazka WHERE id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setInt(1, id);
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            String tytul = resultSet.getString("tytul");
//            String autor = resultSet.getString("autor");
//            ksiazka = new Ksiazka(id, tytul, autor);
//        }
//        return ksiazka;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<Ksiazka> znajdzWszystkie() {
    	
    	Session session = HibernateConfig.getSession();	
    	
    	Query query = session.createQuery("from Ksiazka");
    	List<Ksiazka> ksiazki = query.list();

    	session.close();
    	
    	return ksiazki;
    	
//        List<Ksiazka> ksiazki = new ArrayList<>();
//        Connection connection = database.openConnection();
//        String sql = "SELECT id,tytul,autor FROM ksiazka";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            int id = resultSet.getInt("id");
//            String tytul = resultSet.getString("tytul");
//            String autor = resultSet.getString("autor");
//            Ksiazka ksiazka = new Ksiazka(id, tytul, autor);
//            ksiazki.add(ksiazka);
//        }
//        return ksiazki;
    }

    @Override
    public void usun(int id) {
    	
    	Session session = HibernateConfig.getSession();	
    	
    	session.beginTransaction();
    	Ksiazka ksiazka = znajdz(id);
    	session.delete(ksiazka);
    	session.getTransaction().commit();

    	session.close();    
    	
//        Connection connection = database.openConnection();
//        String sql = "DELETE FROM ksiazka WHERE id=?";
//
//        PreparedStatement statement = connection.prepareStatement(sql);
//        statement.setInt(1, id);
//        statement.executeUpdate();
    }
}
