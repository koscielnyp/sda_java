package fxmlWorkshop;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class LoginController {
	
    @FXML
    private Text textToChangeAfterButtonPress;

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
    	textToChangeAfterButtonPress.setText("Sign in button pressed");
    }
}
