package model;

public class Adres {
	
	private String ulicaNumer;
   	private String kodPocztowy;
   	private String miasto;
   	
   	public String getUlicaNumer() {
		return ulicaNumer;
	}

	public void setUlicaNumer(String ulicaNumer) {
		this.ulicaNumer = ulicaNumer;
	}

	public String getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}
   	
   	Adres() {
   		//nothing to see here, go away
   	}
   	
   	public Adres(String ulicaNumer, String kodPocztowy, String miasto) {
   		this.ulicaNumer = ulicaNumer;
   		this.kodPocztowy = kodPocztowy;
   		this.miasto = miasto;
   	}
   	
   	@Override
   	public String toString() {
   		return ulicaNumer + ", " + kodPocztowy + ", " + miasto;
   	}
}
