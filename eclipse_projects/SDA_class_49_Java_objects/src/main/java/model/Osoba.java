package model;

public class Osoba {
	private String imie;
    private String nazwisko;
    private int wiek;
    private String pesel;
    private Adres aktualnyAdres;

    private Adres[] historiaAdresow = new Adres[10];
	private int index = 0;
    
    Osoba() {
    	//nothing to see here, go away
    }
    
    public Osoba(String imie, String nazwisko) {
    	this.imie = imie;
    	this.nazwisko = nazwisko;
    }
    
    public Osoba(String imie, String nazwisko, int wiek, String pesel) {
    	this.imie = imie;
    	this.nazwisko = nazwisko;
    	this.wiek = wiek;
    	this.pesel = pesel;
    }

	public String getImie() {
		return imie;
	}

	void setImie(String imie) {
    	this.imie = imie;
    }
	
    public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public int getWiek() {
		return wiek;
	}

	public void setWiek(int wiek) {
		this.wiek = wiek;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	
    public void wyswietlDaneOsobowe() {
    	System.out.println("Dane osobowe: "
    						+ imie
    						+ " "
    						+ nazwisko
    						+ ", wiek: "
    						+ wiek
    						+ ", pesel: "
    						+ pesel
    						+ ", adres: "
    						+ aktualnyAdres.getUlicaNumer()
    						+ ", "
    						+ aktualnyAdres.getKodPocztowy()
    						+ ", "
    						+ aktualnyAdres.getMiasto());
    }
    
    public boolean czyPelnoletnia() {
    	  	
    	if (this.wiek >= 18) {
    		return true;
    	} else {
    		return false;
    	}
    	
    }
    
	public void dodajAdres(Adres adres) {
		historiaAdresow[index] = aktualnyAdres;
		index++;
		aktualnyAdres = adres;
	}
	
	public void wyswietlHistorieAdresow() {
		for (Adres kolejnyAdres : historiaAdresow) {
			if (kolejnyAdres != null) {
				System.out.println(kolejnyAdres.getUlicaNumer() + ", " + kolejnyAdres.getKodPocztowy() + ", " + kolejnyAdres.getMiasto());
			}
		}
	}
	
	@Override
   	public String toString() {
    	return  imie
				+ " "
				+ nazwisko
				+ ", wiek: "
				+ wiek
				+ ", pesel: "
				+ pesel
				+ ", adres: "
				+ aktualnyAdres.getUlicaNumer()
				+ ", "
				+ aktualnyAdres.getKodPocztowy()
				+ ", "
				+ aktualnyAdres.getMiasto();
   	}
   	
}
