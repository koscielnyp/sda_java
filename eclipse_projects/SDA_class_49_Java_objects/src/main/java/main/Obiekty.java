package main;

import model.Adres;
import model.Osoba;

public class Obiekty {
    public static void main(String[] args) {
        
    	Osoba osoba1 = new Osoba("Jan", "Kowalski", 18, "4522352352");
        Osoba osoba2 = new Osoba("Janek", "Kowalski", 17, "4522352352");
        Adres adres2 = new Adres( "GdziesTam", "54-613", "Wroclaw");
        Adres adres3 = new Adres( "GdziesIndziej", "42-200", "Czestochowa");
        
        if (osoba2.czyPelnoletnia()) {
        	System.out.println("Pełnoletnia");
        } else {
        	System.out.println("Niepełnoletnia");
        }
        osoba2.dodajAdres(adres2);
        osoba2.wyswietlDaneOsobowe();
        osoba2.dodajAdres(adres3);
        osoba2.dodajAdres(adres2);
        osoba2.wyswietlDaneOsobowe();
        osoba2.wyswietlHistorieAdresow();
        
    }
}
