public class TabliceDwuwymiarowe {

    float znajdzMax(float[][] tablica) {
        
    	int vertLen = 0;
    	int horLen = tablica.length;
    	float max = Integer.MIN_VALUE;
    	
    	for (int i = 0; i < horLen; i++) {
    		
    		vertLen = tablica[i].length;
    		
    		for (int j = 0; j < vertLen; j++) {
    			
    			if (tablica[i][j] > max) {
    				
    				max = tablica[i][j];
    				
    			}
    			
    		}
    		
    	}
    	
    	return max;
    }

    float znajdzMin(float[][] tablica) {
    	
    	int vertLen = 0;
    	int horLen = tablica.length;
    	float min = Integer.MAX_VALUE;
    	
    	for (int i = 0; i < horLen; i++) {
    		
    		vertLen = tablica[i].length;
    		
    		for (int j = 0; j < vertLen; j++) {
    			
    			if (tablica[i][j] < min) {
    				
    				min = tablica[i][j];
    				
    			}
    			
    		}
    		
    	}
    	
    	return min;
    }

    float policzSumeElementow(float[][] tablica) {
    	
    	int vertLen = 0;
    	int horLen = tablica.length;
    	float sum = 0;
    	
    	for (int i = 0; i < horLen; i++) {
    		
    		vertLen = tablica[i].length;
    		
    		for (int j = 0; j < vertLen; j++) {
    			
    			sum += tablica[i][j];
    			
    		}
    		
    	}
    	
    	return sum;
    }
}
