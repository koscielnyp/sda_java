import java.util.Arrays;

public class TabliceJednowymiarowe {

    int policzSumeElementow(int[] tablica) {
    	
    	int len = tablica.length;
    	int sum = 0;
    	for (int i = 0; i < len; i++) {
    		sum += tablica[i];
    	}
    	
        return sum;
    }

    double policzSredniaElementow(int[] tablica) {
       int sum = policzSumeElementow(tablica);
       int num = tablica.length;
       return sum/num;
    }

    void przesunElementyOJedenWPrawo(int[] tablica) {
    	
    	int len = tablica.length;
    	int temp = 0;
    	int last = tablica[len - 1];
    	
    	for (int i = (len - 1); i >= 0; i--) {
    		
    		if (i == 0) {
    			tablica[i] = last;
    		} else {
    			tablica[i] = tablica[i - 1];
    		}
    	}
    	
    }

    void przesunElementyOJedenWLewo(int[] tablica) {
    	
    	int len = tablica.length;
    	int temp = 0;
    	int first = tablica[0];
    	
    	for (int i = 0; i < len; i++) {
    		
    		if ((i + 1) == tablica.length) {
    			tablica[i] = first;
    		} else {
    			tablica[i] = tablica[i + 1];
    		}
    	}
    	
    }

    int znajdzMax(int[] tablica) {
        
    	int len = tablica.length;
    	int max = Integer.MIN_VALUE;
    	
    	for (int i = 0; i < len; i++) {
    		
    		if (tablica[i] > max) {
    			max = tablica[i];
    		}
    		
    	}
    	
    	return max;
    
    }

    int znajdzMin(int[] tablica) {
    	
    	int len = tablica.length;
    	int min = Integer.MAX_VALUE;
    	
    	for (int i = 0; i < len; i++) {
    		
    		if (tablica[i] < min) {
    			min = tablica[i];
    		}
    		
    	}
    	
    	return min;
    }

    void sortuj(int[] tablica) {

		int arraySize = tablica.length;
		
		for (int i = 1; i < arraySize; ++i) {
			for (int j = 1; j < arraySize; j++) {
				if (tablica[j] < tablica[j - 1]) {
					int temp = tablica[j - 1];
					tablica[j - 1] = tablica[j];
					tablica[j] = temp;
				}
			}
		}
    	
    }


}
