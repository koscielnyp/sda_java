import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class TabliceJednowymiaroweTest {

    TabliceJednowymiarowe testowanaKlasa;

    @Before
    public void setUp() throws Exception {
        testowanaKlasa = new TabliceJednowymiarowe();
    }

    @Test
    public void testPoliczSumeElementow() throws Exception {
        int[] tablica = {1, 2, 3, 4, 5};

        int suma = testowanaKlasa.policzSumeElementow(tablica);

        System.out.println("Obliczona suma: " + suma);

        assertEquals("Suma powinna byc rowna 15", 15, suma);
    }

    @Test
    public void testPoliczSredniaElementow() throws Exception {
        int[] tablica = {1, 2, 3, 4, 5};

        double suma = testowanaKlasa.policzSredniaElementow(tablica);

        assertEquals(3, suma, 0.01);
    }

    @Test
    public void testPrzesunElementyOJedenWPrawo() throws Exception {

        int[] tablica = {1, 2, 3, 4, 5};

        testowanaKlasa.przesunElementyOJedenWPrawo(tablica);

        System.out.println("tablica po wykonaniu operacji: " + Arrays.toString(tablica));

        assertEquals(5, tablica[0]);
        assertEquals(1, tablica[1]);
        assertEquals(2, tablica[2]);
        assertEquals(3, tablica[3]);
        assertEquals(4, tablica[4]);
    }

    @Test
    public void testPrzesunElementyOJedenWLewo() throws Exception {
        int[] tablica = {1, 2, 3, 4, 5};

        testowanaKlasa.przesunElementyOJedenWLewo(tablica);

        System.out.println("tablica po wykonaniu operacji: " + Arrays.toString(tablica));

        assertEquals(2, tablica[0]);
        assertEquals(3, tablica[1]);
        assertEquals(4, tablica[2]);
        assertEquals(5, tablica[3]);
        assertEquals(1, tablica[4]);
    }

    @Test
    public void testZnajdzMax() throws Exception {
        int[] tablica = {1, 2, 3, 4, 5};

        int max = testowanaKlasa.znajdzMax(tablica);

        assertEquals(5, max);
    }

    @Test
    public void testZnajdzMin() throws Exception {
        int[] tablica = {1, 2, 3, 4, 5};

        int min = testowanaKlasa.znajdzMin(tablica);

        assertEquals(1, min);
    }
    
    @Test
    public void testSortuj() throws Exception {
        int[] tablica = {3, 1, 5, 4, 2};

        testowanaKlasa.sortuj(tablica);

        System.out.println("tablica po wykonaniu operacji: " + Arrays.toString(tablica));

        assertEquals(1, tablica[0]);
        assertEquals(2, tablica[1]);
        assertEquals(3, tablica[2]);
        assertEquals(4, tablica[3]);
        assertEquals(5, tablica[4]);
    }
}