import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TabliceDwuwymiaroweTest {

    TabliceDwuwymiarowe klasaTestowana;

    @Before
    public void setUp() throws Exception {
       klasaTestowana = new TabliceDwuwymiarowe();
    }

    @Test
    public void testZnajdzMax() throws Exception {

        float[][] tablica = { {1,3}, {4}, {99, 76}};

        float max = klasaTestowana.znajdzMax(tablica);

        assertEquals(99, max, 0.001);
    }

    @Test
    public void testZnajdzMin() throws Exception {
        float[][] tablica = { {1,3}, {4}, {99, 76}};

        float min = klasaTestowana.znajdzMin(tablica);

        assertEquals(1, min, 0.001);
    }

    @Test
    public void testPoliczSumeElementow() throws Exception {
    	
        float[][] tablica = { {1,3}, {4}, {99, 76}};

        float sum = klasaTestowana.policzSumeElementow(tablica);

        assertEquals(183, sum, 0.001);
    }
}