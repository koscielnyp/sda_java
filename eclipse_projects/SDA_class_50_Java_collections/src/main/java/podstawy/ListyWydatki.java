package podstawy;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class ListyWydatki {

	public static void main(String[] args) {
		
		LinkedList<Double> wydatki = new LinkedList<>();
		
		wydatki.add(4.01);
		wydatki.add(2.00);
		wydatki.add(7.64);
		
		//nowy komparator na szybko
		Comparator<Double> komparatorDouble = new Comparator<Double>() {

			@Override
			public int compare(Double o1, Double o2) {
				return (int) (o1.doubleValue() - o2.doubleValue());
			}
			
		};
		
		wydatki.sort(komparatorDouble);
		
		for (double wydatek : wydatki) {
			System.out.println(wydatek);
		}		

	}

}
