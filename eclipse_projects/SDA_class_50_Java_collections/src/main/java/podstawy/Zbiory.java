package podstawy;

import java.util.HashSet;
import java.util.Set;

public class Zbiory {
    public static void main(String[] args) {
    	
    	Set<String> imiona = new HashSet<>();
    	
    	imiona.add("Pawel");
    	imiona.add("Gawel");
    	imiona.add("Zoltan");
    	imiona.add("Pawel");
    	
    	for (String imie : imiona) {
    		System.out.println(imie);
    	}
    	
    	imiona.remove("Gawel");
    	imiona.add("Gawel");
    	
    	for (String imie : imiona) {
    		System.out.println(imie);
    	}   	
    	
    }
}
