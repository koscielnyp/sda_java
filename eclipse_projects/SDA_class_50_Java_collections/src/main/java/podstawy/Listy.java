package podstawy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Listy {
    public static void main(String[] args) {
    		
    	ArrayList<String> imiona = new ArrayList<>();
    	
    	imiona.add("Jan");
    	imiona.add("Jan");
    	imiona.add("Karol");
    	imiona.add("Pawel");
    	
    	//2 sposoby wypisywania z ArrayListy
//    	for (String imie : imiona) {
//    		System.out.println(imie);
//    	}
//    	
//    	for (int i = 0; i < imiona.size(); i++) {
//    		System.out.println(imiona.get(i));
//    	}
    	
//    	imiona.remove("Jan");
    	
    	if (imiona.contains("Jan")) {
    		System.out.println("Istnieje");
    	}
    	
    	Comparator<String> komparator = new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				//Dokladnie odwrocony domyslny komparator
				return o2.compareTo(o1);
			}
		};
    	
    	Collections.sort(imiona, komparator);
    	
    	for (String imie : imiona) {
    		System.out.println(imie);
    	}
    }
}
