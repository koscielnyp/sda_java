import java.util.Random;
import java.util.ArrayList;

public class Main {

	public static void main(String[] arguments) {
		
		Random randomGen = new Random();
		
		//Tablice dwuwymiarowe
		
		System.out.println("Tablica dwuwymiarowa\n");
		
		Integer lengthX = new Integer(6);
		Integer lengthY = new Integer(6);
		
		Integer[][] matrix = new Integer[lengthX][lengthY];
		
		for (int i = 0; i < matrix.length; i++) {
			
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = randomGen.nextInt(5);			
			}
			
		}
		
		String stringToPrint = BasicArrayHelper.twoDimIntArrayToString(matrix);
		System.out.println(stringToPrint);
		
		int[] rowSums = BasicArrayHelper.sumUpRows(matrix);
		
		System.out.println("\nSumy wierszy:");
		for (int i = 0; i < rowSums.length; i++) {
			System.out.print(rowSums[i]);
			if (i < (rowSums.length - 1)) {
				System.out.print(", ");
			}
		}
		
		int[] colSums = BasicArrayHelper.sumUpCols(matrix);
		
		System.out.println("\nSumy kolumn:");
		for (int i = 0; i < colSums.length; i++) {
			System.out.print(colSums[i]);
			if (i < (colSums.length - 1)) {
				System.out.print(", ");
			}
		}
		
		
		
		//Lepsza lista
		
		System.out.println("\n\nArajLista");
		
		ArrayList<Integer> numList = new ArrayList<Integer>();
		
		numList.add(3);
		numList.add(5);
		
		for (int i = 0; i < numList.size(); i++) {
			System.out.print(numList.get(i));
			if (i < (numList.size() - 1)) {
				System.out.print(", ");
			}
		}
		
	}

}