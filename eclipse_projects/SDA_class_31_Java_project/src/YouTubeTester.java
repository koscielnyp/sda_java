import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;


public class YouTubeTester {
	static WebDriver driver;
	static Wait<WebDriver> wait;
	
	public static void main(String[] args) {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 30);
		driver.get("http://www.youtube.com/");

		driver.findElement(By.id("masthead-search-term")).sendKeys("selenium\n");
		//driver.findElement(By.id("search-btn")).click();
		
		
		//Wait for searchbar to complete
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				System.out.println("Searching...");
				return webDriver.findElement(By.id("results")) != null;
			}	
		});
		
        // Look for QAAutomation.net in the results
        boolean isTextFound = driver.findElement(By.tagName("body")).getText()
                .contains("Selenium Tutorial");
        driver.close();
        System.out.println("Test " + (isTextFound? "passed." : "failed."));
	}

}
