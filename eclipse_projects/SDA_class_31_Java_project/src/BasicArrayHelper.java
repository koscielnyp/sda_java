
public class BasicArrayHelper {
	
	public static int[] sumUpRows(Integer[][] array) {
		
		int[] resultArray = new int[array.length];
		
		for (int i = 0; i < array.length; i++) {
			
			int rowSum = 0;
			
			for (int j = 0; j < array[i].length; j++) {
				rowSum += array[i][j];
			}
			
			resultArray[i] = rowSum;
		}
		
		return resultArray;
	}
	
	public static int[] sumUpCols(Integer[][] array) {
		
		int arrayLenX = array.length;
		int arrayLenY = array[arrayLenX - 1].length;
		
		Integer[][] transArray = new Integer[arrayLenX][arrayLenY];
	
		for (int i = 0; i < array.length; ++i){
			for (int j = 0; j < array[i].length; ++j){
				transArray[j][i] = array[i][j];
			}
			
		}
		
		int[] colsSum = BasicArrayHelper.sumUpRows(transArray);
		
		return colsSum;
	}
	
	public static String twoDimIntArrayToString(Integer[][] array) {

		String outputString = new String("");
		
		for (int i = 0; i < array.length; i++) {
			
			int separationLineLength = 0;
					
			for (int j = 0; j < array[i].length; j++) {
				
				outputString += array[i][j];
				separationLineLength += array[i][j].toString().length();
				if (j < (array[i].length - 1)) {
					outputString += "|";
					separationLineLength += 1;
				}				
			}
			
			outputString += "\n";
			
			outputString = createSeparationLine(outputString, separationLineLength);
			
			outputString += "\n";

		}	
		
		return outputString;
	}

	private static String createSeparationLine(String outputString, int j) {
		for (int k = 0; k < j; k++) {
			outputString += "-";
		}
		return outputString;
	}
	
}
