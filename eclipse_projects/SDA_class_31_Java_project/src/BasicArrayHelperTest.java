import static org.junit.Assert.*;

import org.junit.Test;

public class BasicArrayHelperTest {

	@Test
	public void testSumUpRowsLength() {
		Integer[][] testArray = {{1,2,3},{3,2,1},{1}};
		int expectedResultLength = 3;
		
		int actualResultLength = BasicArrayHelper.sumUpRows(testArray).length;
		assertEquals("Dlugosc wyniku nie jest poprawa", expectedResultLength, actualResultLength);
	}
	
	@Test
	public void testSumUpRowsSum() {
		Integer[][] testArray = {{1,2,3},{3,4,5},{1}};
		int[] expectedResultSum = {6,12,1};
		int[] actualResultSum = BasicArrayHelper.sumUpRows(testArray);
		
		for (int i = 0; i < expectedResultSum.length; i++) {
			
			int expectedRowSum = expectedResultSum[i];
			int actualRowSum = actualResultSum[i];
			assertEquals("Wartosci sie nie zgadzaja", expectedRowSum, actualRowSum);
		}
		
	}
	
	@Test
	public void testSumUpColsSum() {
		Integer[][] testArray = {{1,2,3},{3,4,5},{1,2,3}};
		int[] expectedResultSum = {5,8,11};
		int[] actualResultSum = BasicArrayHelper.sumUpCols(testArray);
		
		for (int i = 0; i < expectedResultSum.length; i++) {
			
			int expectedColSum = expectedResultSum[i];
			int actualColSum = actualResultSum[i];
			assertEquals("Wartosci sie nie zgadzaja", expectedColSum, actualColSum);
		}
		
	}

}
