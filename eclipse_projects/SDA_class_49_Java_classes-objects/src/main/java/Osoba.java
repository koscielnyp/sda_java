public class Osoba {
    String imie;
    String nazwisko;
    long pesel;

    Osoba(String imie, String nazwisko, long pesel) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
    }
    
    @Override
    public boolean equals(Object obj) {
    	
    	if (obj == null) {
    		return false;
    	} else {
    		
	    	Osoba osobaToCompare = (Osoba) obj;
	    	
	    	if (this.imie.equals(osobaToCompare.imie) && this.nazwisko.equals(osobaToCompare.nazwisko) && this.pesel == osobaToCompare.pesel){
	    		return true;
	    	} else {
	    		return false;
	    	}
	    	
    	}
    }
    
    @Override
    public int hashCode() {
    	
    	return (int) imie.hashCode() + (int) pesel; 
    	
        /*int result = imie != null ? imie.hashCode() : 0;
        result = 31 * result + (nazwisko != null ? nazwisko.hashCode() : 0);
        result = 31 * result + (int) (pesel ^ (pesel >>> 32));
        return result;*/
    	
    }
}
