import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OsobaTest {

    Osoba osoba1;
    Osoba osoba2;
    Osoba osoba3;
    Osoba osoba4;

    @Before
    public void setUp() throws Exception {

        osoba1 = new Osoba("Jan", "Kowalski", 9508147894L);
        osoba2 = new Osoba("Jan", "Kowalski", 9508147894L);
        osoba3 = new Osoba("Jan", "Kowalski", 9508147894L);
        osoba4 = new Osoba("Marek", "Nowak", 92041309888L);
    }

    // Testy kontraktu equals

    @Test
    public void testEqualsZwrotna() throws Exception {
        assertTrue(osoba1.equals(osoba1));
        assertTrue(osoba2.equals(osoba2));
        assertTrue(osoba3.equals(osoba3));
        assertTrue(osoba4.equals(osoba4));
    }

    @Test
    public void testEqualsSymetryczna() throws Exception {
        assertTrue(osoba1.equals(osoba2) == osoba2.equals(osoba1));
        assertTrue(osoba1.equals(osoba3) == osoba3.equals(osoba1));
        assertTrue(osoba1.equals(osoba4) == osoba4.equals(osoba1));
    }

    @Test
    public void testEqualsPrzechodnia() throws Exception {
        if (osoba1.equals(osoba2) && osoba2.equals(osoba3)) {
            assertTrue(osoba1.equals(osoba3));
        }
    }

    @Test
    public void testEqualsKonsekwentna() throws Exception {
        boolean wynik = osoba1.equals(osoba3);

        for (int i = 0; i < 100; i++) {
            assertEquals(wynik, osoba1.equals(osoba3));
        }
    }

    @Test
    public void testEqualsNull() throws Exception {
        assertFalse(osoba1.equals(null));
    }

    // Testy sprawdzaj�ce r�wno�� obiekt�w

    @Test
    public void testRownosc() throws Exception {
        assertTrue(osoba1.equals(osoba2));
        assertTrue(osoba1.equals(osoba3));
        assertFalse(osoba1.equals(osoba4));
    }

    // Testy sprawdzajace kontrakt hashCode

    @Test
    public void testHashCodeKonsekwentna() throws Exception {
        int hashCode = osoba1.hashCode();

        for (int i = 0; i < 100; i++) {
            assertEquals(hashCode, osoba1.hashCode());
        }
    }

    @Test
    public void testHashCodeZgodneZEquals() throws Exception {
        if (osoba1.equals(osoba2)) {
            assertTrue(osoba1.hashCode() == osoba2.hashCode());
        }
    }


}