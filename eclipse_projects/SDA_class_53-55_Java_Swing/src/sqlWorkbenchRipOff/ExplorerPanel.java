package sqlWorkbenchRipOff;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

public class ExplorerPanel extends JPanel implements ActionListener{
	
	JComboBox<String> schemaList;
	JList<String> tableList;
	
	public ExplorerPanel() {		
		super(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		
		//Combo Box
		String[] schemas = getSchemas();
		this.schemaList = new JComboBox<String>(schemas);
		
		this.schemaList.addActionListener(this);
		
		this.tableList = new JList<String>();
		
		//ComboBox
		c.gridx = 0; c.gridy = 0; c.anchor = GridBagConstraints.FIRST_LINE_START; c.fill = GridBagConstraints.VERTICAL; c.weighty = 0;
		this.add(this.schemaList, c);
		
		//schema list - konfigurujemy gdzie i jak wstawimy list� tabel
		c.gridy = 1;c.fill = GridBagConstraints.BOTH; c.weighty = 1;
		this.add(this.tableList, c);		
		
	}

	private String[] getTables(String schema) {
		if (schema.equals("sakila")) {
			String[] tables = {"customer", "film", "rental"};
			return tables;
		} else if (schema.equals("sda")) {
			String[] tables = {"user", "ticket"};
			return tables;
		} else {
			String[] tables = {"columns", "column_privileges"};
			return tables;
		}
	}

	private String[] getSchemas() {
		String[] schemaList = {"sakila", "sda", "information_schema"};
		return schemaList;
	}
	
//	public static void main(String[] args) {
//		JFrame frame = new JFrame("Better than SQL Workbench");
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setMinimumSize(new Dimension(500,500));	
//		
//		ExplorerPanel ep = new ExplorerPanel();
//		frame.setContentPane(ep);
//		
//		frame.pack();
//		frame.setVisible(true);
//	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JComboBox<String> clickedComboBox = (JComboBox<String>) e.getSource();
		String selectedSchema = (String) clickedComboBox.getSelectedItem();
		String[] tableArray = getTables(selectedSchema);
		tableList.setListData(tableArray);
		
	}
}
