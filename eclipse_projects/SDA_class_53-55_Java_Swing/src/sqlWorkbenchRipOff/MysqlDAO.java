package sqlWorkbenchRipOff;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MysqlDAO {
	
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;

	//otwarcie polaczenia
	public void connect() {
		try {
			connect = DriverManager.getConnection("jdbc:mysql://localhost/sakila?" + "user=root&password=root");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//zamkniecie polaczenia
	public void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}
	
	//odpalenie query i zapisanie go do resultSet
	public ResultSet executeQuery(String query) {
		try {		

			statement = connect.createStatement();
			resultSet = statement.executeQuery(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return resultSet;

	}
	
	//wyciaga liste columns z query z metody executeQuery
	public static String[] getColumnsFromResultSet(ResultSet resultSet) throws SQLException {
		
		int numColumns = resultSet.getMetaData().getColumnCount();
		String[] columns = new String[numColumns];

		for (int i = 1; i <= numColumns; i++) {
			columns[i - 1] = resultSet.getMetaData().getColumnName(i);
		}
		
		return columns;
	}
	
	//wyciaga wiersze z query z metody executeQuery
	public static String[][] getDataFromResultSet(ResultSet resultSet) throws SQLException {
		
		int numColumns = resultSet.getMetaData().getColumnCount();
		ArrayList<String[]> rowsList = new ArrayList<String[]>();
		
		while (resultSet.next()) {
			String[] currentRow = new String[numColumns];
			for (int i = 1; i <= numColumns; i++) {
				currentRow[i - 1] = resultSet.getString(i);
			}
			rowsList.add(currentRow);
		}
		
		return rowsList.toArray(new String[rowsList.size()][numColumns]);
	}

//	public static void main(String[] args) throws SQLException {
//		
//		MysqlDAO dao = new MysqlDAO();
//		dao.connect();
//		ResultSet resultSet = dao.executeQuery("SELECT * FROM CUSTOMER LIMIT 10");
//		
//		int numColumns = resultSet.getMetaData().getColumnCount();
//		
//		for (int i = 1; i <= numColumns; i++) {
//			System.out.print(resultSet.getMetaData().getColumnName(i) + " | ");
//		}
//		System.out.println();
//		
//		while(resultSet.next()) {
//			for (int i = 1; i <= numColumns; i++) {
//				System.out.print(resultSet.getString(i) + " | ");
//			}
//			System.out.println();
//			
//		}
//		
//		dao.close();
//	}

}
