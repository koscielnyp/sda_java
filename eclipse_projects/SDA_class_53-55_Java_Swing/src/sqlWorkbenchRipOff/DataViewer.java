package sqlWorkbenchRipOff;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DataViewer extends JPanel {
	
	JTable dataTable;
	JScrollPane dataScrollPane;
	
//	stary constructor
//	public DataViewer(String query) throws SQLException {
//		
//		populateDataTableFromQuery(query);
//		dataScrollPane = new JScrollPane(dataTable);
//		
//	}
	
	//constructor
	public DataViewer() {
		super(new GridLayout(1,1));
		
		this.dataTable = new JTable();
		
		this.dataTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
		this.dataTable.setFillsViewportHeight(true);
		
		this.dataScrollPane = new JScrollPane(this.dataTable);	
		this.add(this.dataScrollPane);
		
	}
	
	public void updateDataTableFromQuery(String query) throws SQLException {
		
		MysqlDAO dao = new MysqlDAO();
		dao.connect();
		ResultSet resultSet = dao.executeQuery(query);
		
		dataTable.setModel(new DefaultTableModel(MysqlDAO.getDataFromResultSet(resultSet), MysqlDAO.getColumnsFromResultSet(resultSet)));
		
		dao.close();
		
	}
	
//	public static void main(String[] args) throws SQLException {
//		JFrame frame = new JFrame("Better than SQL Workbench");
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setMinimumSize(new Dimension(500,500));
//		
//		DataViewer dv = new DataViewer();
//		dv.updateDataTableFromQuery("SELECT * FROM CUSTOMER");
//		
//		frame.getContentPane().add(dv, BorderLayout.CENTER);
//	
//		JButton button = new JButton("Change Data");
//		button.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				
//				try {
//					dv.updateDataTableFromQuery("SELECT * FROM FILM");
//				} catch (SQLException e1) {
//					e1.printStackTrace();
//				}
//				
//			}
//		});
//		
//		frame.getContentPane().add(button, BorderLayout.PAGE_END);
//		
//		frame.pack();
//		frame.setVisible(true);
//	}
}
