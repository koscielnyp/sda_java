package sqlWorkbenchRipOff;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SqlWorkbenchRipOff extends JPanel implements ListSelectionListener{

	ExplorerPanel explorerPanel;
	DataViewer dataViewer;
	
	public SqlWorkbenchRipOff() {
		super(new GridBagLayout());
		explorerPanel = new ExplorerPanel();
		dataViewer = new DataViewer();
		
		//zakladamy listener na liscie tabel
		explorerPanel.tableList.addListSelectionListener(this);
		
		
		//Ustawienie wygladu i pozycji dla elementow
		GridBagConstraints c = new GridBagConstraints();	
		c.gridx = 0; c.gridy = 0; c.anchor = GridBagConstraints.FIRST_LINE_START; c.fill = GridBagConstraints.VERTICAL; c.weighty = 1;
		this.add(this.explorerPanel, c);
		c.gridx = 1; c.weightx = 1; c.weighty = 1; c.fill = GridBagConstraints.BOTH; 
		this.add(this.dataViewer, c);
		
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("Better than SQL Workbench");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500,500));	
		
		SqlWorkbenchRipOff mainWindow = new SqlWorkbenchRipOff();
		frame.setContentPane(mainWindow);
		
		frame.pack();
		frame.setVisible(true);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (!e.getValueIsAdjusting()) { //zapobiega podwojnemu wykonaniu
			JList clickedList = (JList) e.getSource();
			String currentTable = (String) clickedList.getSelectedValue();
			String currentSchema = (String) explorerPanel.schemaList.getSelectedItem();
			if (currentTable != null) {
				//System.out.println("Select * from " + currentSchema + "." + currentTable);
				String query = "SELECT * FROM " + currentSchema + "." + currentTable;
				try {
					dataViewer.updateDataTableFromQuery(query);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		
	}

}
