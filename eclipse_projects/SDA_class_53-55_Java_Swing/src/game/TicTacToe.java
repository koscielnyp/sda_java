package game;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TicTacToe extends JPanel implements ActionListener {
	
	private boolean crossNow = true;
	private int counter = 0;
	
	public TicTacToe() {
		super(new GridLayout(3, 3, 10, 10));
		addButtons();
	}
	
	private void addButtons() {
		
		for (int i = 0; i < 9; i++) {
			
			JButton button = new JButton();
			button.setActionCommand("Button" + i);
			button.addActionListener(this);
			add(button);
			
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton currentButton = (JButton) e.getSource();
		if (currentButton.getText().equals("")) {
			if (crossNow) {
				currentButton.setText("X");
			} else {
				currentButton.setText("O");
			}
			crossNow = !crossNow;
			counter++;
		}

		
		System.out.println(e.getActionCommand());
		
		if (counter == 9) {
			JOptionPane.showMessageDialog(this, "Koniec gry!");
		}
		
	}
	
	private static void createAndShowGUI() {
		
		JFrame frame = new JFrame("Kolko i Krzyzyk");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500,500));
		
		TicTacToe gamePanel = new TicTacToe();
		frame.setContentPane(gamePanel);
		
		frame.pack();
		frame.setVisible(true);
		
	}
	
	public static void main(String[] args) {
		//Schedule a job for the event-dispatching
		thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
				
			public void run() {
				createAndShowGUI();
			}
				
		});
	
	}
	
}
