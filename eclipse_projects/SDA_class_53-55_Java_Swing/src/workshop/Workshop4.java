package workshop;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Workshop4 {
	
	public static void main(String[] args) {
		//Schedule a job for the event-dispatching
		thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
				
			public void run() {
				createAndShowGUI();
			}
				
		});
	
	}
	
	private static void createAndShowGUI() {
		
		JFrame frame = new JFrame("Hello Java!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500,100));
		
		JMenuBar greenMenuBar = new JMenuBar();
		greenMenuBar.setOpaque(true);
		greenMenuBar.setBackground(new Color(154, 165, 127));
		greenMenuBar.setPreferredSize(new Dimension(500, 20));
		
		JMenu fileMenu = new JMenu("File");
		JMenu aboutMenu = new JMenu("About");
		
		greenMenuBar.add(fileMenu);
		greenMenuBar.add(aboutMenu);
		
		//MENU ITEMS
		JMenuItem openFileMenuItem = new JMenuItem("Open");
		JMenuItem exitFileMenuItem = new JMenuItem("Exit");
		fileMenu.add(openFileMenuItem);
		fileMenu.add(exitFileMenuItem);
		
		JMenuItem authorsAboutMenuItem = new JMenuItem("Authors");
		aboutMenu.add(authorsAboutMenuItem);
		
		//Ustaw pasek jako pasek stworzonego okna
		frame.setJMenuBar(greenMenuBar);


		JLabel loginLabel = new JLabel("Login:");
		JLabel passLabel = new JLabel("Password:");
		JLabel logLabel = new JLabel("Place for logs");
		JTextField textField = new JTextField(10);
		JPasswordField passwordField = new JPasswordField(10);
		JButton loginButton = new JButton("Send");
		
		JPanel loginPane = new JPanel();
		loginPane.setLayout(new GridBagLayout());
		Insets insets = new Insets(0, 0, 0, 0);
		
		loginPane.add(loginLabel, new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, insets, 0, 0));
		loginPane.add(passLabel, new GridBagConstraints(0, 1, 1, 1, 0.5, 0.5, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, insets, 0, 0));
		
		frame.getContentPane().add(loginPane, BorderLayout.CENTER);
			
		frame.pack();
		frame.setVisible(true);
		
	}
	
}
	
