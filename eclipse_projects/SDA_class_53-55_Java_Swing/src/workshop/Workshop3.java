package workshop;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Workshop3 implements ActionListener {
	
	private JLabel logLabel;
	
	public static void main(String[] args) {
		//Schedule a job for the event-dispatching
		thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
				
			public void run() {
				Workshop3 workshop = new Workshop3();
				workshop.createAndShowGUI();
			}
				
		});
	
	}
	
	private void createAndShowGUI() {
		
		JFrame frame = new JFrame("Hello Java!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500,100));
		
		JMenuBar greenMenuBar = new JMenuBar();
		greenMenuBar.setOpaque(true);
		greenMenuBar.setBackground(new Color(154, 165, 127));
		greenMenuBar.setPreferredSize(new Dimension(500, 20));
		
		JMenu fileMenu = new JMenu("File");
		JMenu aboutMenu = new JMenu("About");
		
		greenMenuBar.add(fileMenu);
		greenMenuBar.add(aboutMenu);
		
		//MENU ITEMS
		JMenuItem openFileMenuItem = new JMenuItem("Open");
		JMenuItem exitFileMenuItem = new JMenuItem("Exit");
		fileMenu.add(openFileMenuItem);
		fileMenu.add(exitFileMenuItem);
		
		JMenuItem authorsAboutMenuItem = new JMenuItem("Authors");
		aboutMenu.add(authorsAboutMenuItem);
		
		//Ustaw pasek jako pasek stworzonego okna
		frame.setJMenuBar(greenMenuBar);


		JLabel loginLabel = new JLabel("Login:");
		JLabel passLabel = new JLabel("Password:");
		logLabel = new JLabel("Place for logs");
		JTextField textField = new JTextField(10);
		JPasswordField passwordField = new JPasswordField(10);
		JButton loginButton = new JButton("Send");
		loginButton.addActionListener(this);
		
		JPanel loginPaneFirstColumn = new JPanel();		
		loginPaneFirstColumn.setLayout(new BoxLayout(loginPaneFirstColumn, BoxLayout.PAGE_AXIS));
		
		loginPaneFirstColumn.add(loginLabel);
		loginPaneFirstColumn.add(passLabel);
		
		JPanel loginPaneSecondColumn = new JPanel();		
		loginPaneSecondColumn.setLayout(new BoxLayout(loginPaneSecondColumn, BoxLayout.PAGE_AXIS));
		
		loginPaneSecondColumn.add(textField);
		loginPaneSecondColumn.add(passwordField);
		
		JPanel loginPaneLastColumn = new JPanel();		
		loginPaneLastColumn.setLayout(new BoxLayout(loginPaneLastColumn, BoxLayout.PAGE_AXIS));
		
		loginPaneLastColumn.add(loginButton);
		loginPaneLastColumn.add(logLabel);
		
		frame.getContentPane().add(loginPaneFirstColumn, BorderLayout.LINE_START);
		frame.getContentPane().add(loginPaneSecondColumn, BorderLayout.CENTER);
		frame.getContentPane().add(loginPaneLastColumn, BorderLayout.PAGE_END);
			
		frame.pack();
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		logLabel.setText("Proba logowania: " + new Date());
		
	}
	
}
	
