package workshop;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;

public class Workshop2 {
	
	public static void main(String[] args) {
		//Schedule a job for the event-dispatching
		thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
				
			public void run() {
				createAndShowGUI();
			}
				
		});
	
	}
	
	private static void createAndShowGUI() {
		
		JFrame frame = new JFrame("Hello Java!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500,300));
		
		JMenuBar greenMenuBar = new JMenuBar();
		greenMenuBar.setOpaque(true);
		greenMenuBar.setBackground(new Color(154, 165, 127));
		greenMenuBar.setPreferredSize(new Dimension(500, 20));
		
		JMenu fileMenu = new JMenu("File");
		JMenu aboutMenu = new JMenu("About");
		
		greenMenuBar.add(fileMenu);
		greenMenuBar.add(aboutMenu);
		
		//MENU ITEMS
		JMenuItem openFileMenuItem = new JMenuItem("Open");
		JMenuItem exitFileMenuItem = new JMenuItem("Exit");
		fileMenu.add(openFileMenuItem);
		fileMenu.add(exitFileMenuItem);
		
		JMenuItem authorsAboutMenuItem = new JMenuItem("Authors");
		aboutMenu.add(authorsAboutMenuItem);
		
		//Ustaw pasek jako pasek stworzonego okna
		frame.setJMenuBar(greenMenuBar);

		JRadioButton firstButton = new JRadioButton("First Button");
			firstButton.setMnemonic(KeyEvent.VK_1);
			firstButton.setSelected(true);

				
		JRadioButton secondButton = new JRadioButton("Second Button");
			secondButton.setMnemonic(KeyEvent.VK_2);

						
		JRadioButton thirdButton = new JRadioButton("Third Button");
			thirdButton.setMnemonic(KeyEvent.VK_3);
		
		ButtonGroup group = new ButtonGroup();
		group.add(firstButton);
		group.add(secondButton);
		group.add(thirdButton);
		
		frame.getContentPane().add(firstButton, BorderLayout.PAGE_START);
		frame.getContentPane().add(secondButton, BorderLayout.CENTER);
		frame.getContentPane().add(thirdButton, BorderLayout.PAGE_END);
			
		frame.pack();
		frame.setVisible(true);
		
	}
	
}
	
