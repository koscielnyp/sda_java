package workshop;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Workshop1 {
	
	public static void main(String[] args) {
		//Schedule a job for the event-dispatching
		thread:
		//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
				
			public void run() {
				createAndShowGUI();
			}
				
		});
	
	}
	
	private static void createAndShowGUI() {
		
		JFrame frame = new JFrame("Hello Java!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500,300));
		
		JMenuBar greenMenuBar = new JMenuBar();
		greenMenuBar.setOpaque(true);
		greenMenuBar.setBackground(new Color(154, 165, 127));
		greenMenuBar.setPreferredSize(new Dimension(500, 20));
		
		JMenu fileMenu = new JMenu("File");
		JMenu aboutMenu = new JMenu("About");
		
		greenMenuBar.add(fileMenu);
		greenMenuBar.add(aboutMenu);
		
		//MENU ITEMS
		JMenuItem openFileMenuItem = new JMenuItem("Open");
		JMenuItem exitFileMenuItem = new JMenuItem("Exit");
		fileMenu.add(openFileMenuItem);
		fileMenu.add(exitFileMenuItem);
		
		JMenuItem authorsAboutMenuItem = new JMenuItem("Authors");
		aboutMenu.add(authorsAboutMenuItem);
		
		//Ustaw pasek jako pasek stworzonego okna
		frame.setJMenuBar(greenMenuBar);

		JLabel label = new JLabel("Hello Java!", JLabel.CENTER);
		label.setFont(new Font(label.getFont().getName(), label.getFont().getStyle(), 30));
		
		JLabel picture = new JLabel(new ImageIcon("resources" + File.separator + "Java_logo.png"));
//		JLabel mix = new JLabel("HelloSwing!", new ImageIcon("resources" + File.separator + "hello.jpg"), JLabel.LEFT);
		
		frame.getContentPane().add(picture, BorderLayout.LINE_START);
		frame.getContentPane().add(label, BorderLayout.CENTER);
//		frame.getContentPane().add(mix, BorderLayout.LINE_END);

		//JButton
		JButton firstButton = new JButton("Don't push");
		frame.getContentPane().add(firstButton, BorderLayout.PAGE_END);
		
		frame.pack();
		frame.setVisible(true);
		
	}
	
}
	
