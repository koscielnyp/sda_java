//RPN - Reverse Polish Notation
public class myRPN {
	
	public static int RPN(String expr) {
		
		String[] tokens = expr.split(" ");
		
		arturStack stack = new arturStack();
		
		int result = -1;
		
		//Set paramenters in inversed order
		for (int i = 0; i < tokens.length; i++) {
			stack.Push(tokens[(tokens.length - 1) - i]);
		}
		
		while (!stack.isOnlyOneLeft()) {
			computeLastThree(stack);
		}
		
		result = Integer.parseInt(stack.Pop().toString());
		return result;
		
	}
	
	private static void computeLastThree(arturStack stack) {

		int result = -1;
		int argB = Integer.parseInt(stack.Pop().toString());
		int argA = Integer.parseInt(stack.Pop().toString());
		String oper = stack.Pop().toString();
		
		if (oper.equals("+")) {
			result = argA + argB;
		}

		if (oper.equals("-")) {
			result = argA - argB;
		}
		
		if (oper.equals("*")) {
			result = argA * argB;
		}
		
		if (oper.equals("/")) {
			result = argA / argB;
		}
		
		stack.Push(result);
			
	}

}
