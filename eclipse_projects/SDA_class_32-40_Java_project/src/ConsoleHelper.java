import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class ConsoleHelper {

	//Sprawdzanie metod
	static {
		assert writeDownWelcome("Pawe�").equals("Cze��, jestem Pawe�"): "writeDownWelcome nie dzia�a jak nale�y";
		assert getExtremeValue("max", new int[] {1, 2, 4, 5, 67, 3, 4}) == 67: "getExtremeValue z max nie dzia�a jak nale�y";
		assert getExtremeValue("min", new int[] {1, 2, 4, 5, 67, 3, 4}) == 1: "getExtremeValue z min nie dzia�a jak nale�y";
		assert getMaxValueRecur(new ArrayList<>(Arrays.asList(1, 2, 4, 5, 67, 3, 4))) == 67: "getMaxValueRecur nie dzia�a jak nale�y";
		assert getMaxValueRecurAlt(new ArrayList<>(Arrays.asList(1, 2, 4, 5, 67, 3, 4)), 0) == 67: "getMaxValueRecurAlt nie dzia�a jak nale�y";
		assert sumMultiplies(new int[] {3, 5}, 10) == 23: "sumMultiplies nie dzia�a jak nale�y";
		assert fibbNotRecur(12) == 144: "fibbNotRecur nie dzia�a jak nale�y";
		assert fibbRecur(12) == 144: "fibbRecur dzia�a jak nale�y";
		assert fibbSumEvenSummingCorrectParameterAsArray(4000000) == 4613732: "fibbSumEvenSummingCorrectParameterAsArray nie dzia�a jak nale�y";
		assert fibbSumEvenSummingCorrectParameterAsQueue(4000000) == 4613732: "fibbSumEvenSummingCorrectParameterAsQueue nie dzia�a jak nale�y";
		assert findBiggestPalindrome(99,99) == 9009: "findBiggestPalindrome nie dziala jak nalezy";
		assert findMinValDivByRange(1, 10) == 2520: "findMinValDivByRange nie dziala jak nalezy";
		assert expoSumExpo(10) == 2640: "expoSumExpo nie dziala jak nalezy";
		assert isPrime(97) == true: "isPrime nie dziala jak nalezy";
		assert isPrime(2) == true: "isPrime nie dziala jak nalezy";
		assert isPrime(3) == true: "isPrime nie dziala jak nalezy";
		assert isPrime(13) == true: "isPrime nie dziala jak nalezy";
		assert isPrime(14) == false: "isPrime nie dziala jak nalezy";
		assert nthPrime(10001) == 104743: "isPrime nie dziala jak nalezy";
		assert nthPrime(1) == 2: "isPrime nie dziala jak nalezy";
	}
	
	public static String writeDownWelcome(String name) {
		
		String welcome = "Cze��, jestem " + name;
		return welcome;
	}
	
	public static int getExtremeValue(String operation, int[] array) {
		
		int extremeValue = array[0];
		int arrayLen = array.length;
		
		if (operation == "max") {
			for (int i = 1; i < arrayLen; i++) {
				if (array[i] > extremeValue) {
					extremeValue = array[i];
				}
			}
		}
		
		if (operation == "min") {
			for (int i = 1; i < arrayLen; i++) {
				if (array[i] < extremeValue) {
					extremeValue = array[i];
				}
			}
		}
		
		return extremeValue;
	}
	
	
	//Overload getMaxValueRecur
	public static int getMaxValueRecur(ArrayList<Integer> array) {
		return getMaxValueRecur(array, Integer.MIN_VALUE);
	}
	
	public static int getMaxValueRecur(ArrayList<Integer> array, int currentMax) {
		
		if (array.size() > 0) {
			if (array.get(0) > currentMax) {
				currentMax = array.get(0);
				array.remove(0);
				return getMaxValueRecur(array, currentMax);
			}
		}
		
		return currentMax;
	}
	

	//Overload getMaxValueRecurAlt
	public static int getMaxValueRecurAlt(List<Integer> array, int start) {
		return getMaxValueRecurAlt(array, start, Integer.MIN_VALUE);
	}
	
	public static int getMaxValueRecurAlt(List<Integer> array, int start, int currentMax) {
		
		if ( start == array.size() ) {
			return currentMax;
		} else {
			return getMaxValueRecurAlt(array, start + 1, Math.max(currentMax, array.get(start)));
		}
	}
	
	
	//to nie dziala jak trzeba bo chyba nie zrozumialem specyfikacji ;)
	public static int sumMultiplies(int[] array, int countTo) {
		
		int arrayLen = array.length;
		int finalSum = 0;
		
		for (int i = 0; i < arrayLen; ++i) {
			if (array[i] == 0) {
				finalSum += 0;
			} else {
				int currentSubSum = 0;
				while (currentSubSum < (countTo - array[i])) {
					currentSubSum += array[i];
					finalSum += currentSubSum;
				}
				
			}
		}
		
		//Tu musi byc cos wylicza NWW, wylicza wielokrotnosc tego NWW az do CountTo i odejmuje od finalSum
		
		return finalSum;
	}
	
	
	public static int fibbNotRecur(int limit) {
		
		ArrayList<Integer> array = new ArrayList<>(Arrays.asList(1, 1));

		for (int i = 2; i <= limit; i++) {
			
			/*int temp = array.get(0) + array.get(1);
			array.add(temp);*/
			array.add(array.get(0) + array.get(1)); //tak jest krocej
			array.remove(0);
			//tablica ma w efekcie zawsze max 3 elementy - oszczednosc pamieciowa, i think
		}
		
		return array.get(0);
	}
	
	
	public static long fibbRecur(int limit) {
		
		long result;
		
		if (limit <= 1) { //wczesniej bylo osobne sprawdzanie dla 0 i 1, ale bylo 0 = 0 i 1 = 1 wiec mozna bylo to skrocic o 1 warunek
			result = limit;
		} else {
			result = fibbRecur(limit - 1) + fibbRecur(limit - 2);
		}
			
		return result;
	}
	
	public static int fibbSumEvenSummingCorrectParameterAsArray(int limit) {
		
		ArrayList<Integer> array = new ArrayList<>(Arrays.asList(1, 1));
		int currentSum = 0;
		
		do {
			array.add(array.get(0) + array.get(1));
				if (array.get(2) % 2 == 0) {
					currentSum += array.get(2);
				}
			array.remove(0);
		} while (array.get(1) < limit);
		
		return currentSum;
	}
	
	public static int fibbSumEvenSummingCorrectParameterAsQueue(int limit) {
		
		Deque<Integer> queue = new ArrayDeque<Integer>(Arrays.asList(1, 1));
		
		int currentSum = 0;
		
		do {
			queue.add(queue.getFirst() + queue.getLast());
			if (queue.getLast() % 2 == 0) {
				currentSum += queue.getLast();
			}
			queue.removeFirst();
		} while (queue.getLast() < limit);
		return currentSum;
	}
	
	public static long findBiggestPalindrome(int a, int b) {
		
		long highestPalindrome = -1;
		
		for (int i = 1; i <= a; i++) {
			for (int j = 1; j <= b; j++) {
				long multiplyVal = i * j;
				String multiplyValStr = Long.toString(multiplyVal);
				int multiplyValStrEffectiveLen = multiplyValStr.length() - 1;
				int multiplyValStrEffectiveLenHalf = (multiplyValStr.length()/2) - 1;
				boolean isCurrentAPalindrome = true;
				for (int k = 0; k <= multiplyValStrEffectiveLenHalf; k++) {
					if ((multiplyValStr.charAt(k) != (multiplyValStr.charAt(multiplyValStrEffectiveLen - k))) && isCurrentAPalindrome == true) {
						isCurrentAPalindrome = false;
					}
				}
				if (multiplyVal > highestPalindrome && isCurrentAPalindrome == true) {
					highestPalindrome = multiplyVal;
				}
			}
		}
		
		return highestPalindrome;
	}
	
	public static int findMinValDivByRange(int rangeFrom, int rangeTo) {
		
		int minDivVal = -1;
		int currVal = 1;
		boolean divByAll = false;
		boolean currDivByAll = true;
		
		while (currVal <= Integer.MAX_VALUE && divByAll == false) {
			
			for (int i = rangeFrom; i <= rangeTo; i++) {
				
				if (i <= currVal && currDivByAll == true) {
					if (currVal % i != 0) {
						currDivByAll = false;
					}				
					if (i == rangeTo && currDivByAll == true) {
						minDivVal = currVal;
						divByAll = true;
					}						
				}			
			}
			currVal++;
			currDivByAll = true;
		}
		
		return minDivVal;
			
	}
	
	//roznica kwadratu sumy i sumy kwadratow
	public static long expoSumExpo (int rangeTo) {
		
		long sumExpo = 0;
		long expoSum = 0;
		long sumSub = 0;
		
		for (int i = 1; i <= rangeTo; i++) {
			sumExpo += i*i;
			expoSum += i;
		}
		expoSum = expoSum * expoSum;
		
		sumSub = expoSum - sumExpo;	
		return sumSub;
		
	}
	
	public static boolean isPrime(int value) {
		
		boolean isPrime = true;
			
		for (int i = 2; i <= (value - 1); i++) {
			if (value % i == 0) {
				isPrime = false;
				break;
			}
		}
		
		return isPrime;
	}
	
	public static long nthPrime(int n) {
		
		int numOfPrimes = 0;
		long currHighestPrime = 1;
		for (int i = 1; numOfPrimes <= n; i++) {
			if (isPrime(i) == true) {
				numOfPrimes++;
				currHighestPrime = i;
			}
		}
		return currHighestPrime;
		
	}

}
