
public class myBubbleSort {

	public static int[] sort(int[] array) {
		
		//My JavaScript implementation from zajecia 11
        /*var sortArray = function(array) {
            
            var arraySize = array.length;
            
            for (var i = 1; i < arraySize; ++i) {
                for (var i2 = 1; i2 < arraySize; ++i2) {
                    if (array[i2] < array[i2 - 1]) {
                        var temp = array[i2 - 1];
                            array[i2 - 1] = array[i2];
                            array[i2] = temp;
                    }
                }
            }
            return array;
        };*/
		
		int arraySize = array.length;
		
		for (int i = 1; i < arraySize; ++i) {
			for (int j = 1; j < arraySize; j++) {
				if (array[j] < array[j - 1]) {
					int temp = array[j - 1];
					array[j - 1] = array[j];
					array[j] = temp;
				}
			}
		}
		
		return array;
	}
	
	public static String typeAll (int[] array) {
		
		int arraySize = array.length;
		String str = "";
		
		for (int i = 0; i < arraySize; ++i) {
			str += Integer.toString(array[i]);
			str += " ";
		}
		return str;
	}
	
}
