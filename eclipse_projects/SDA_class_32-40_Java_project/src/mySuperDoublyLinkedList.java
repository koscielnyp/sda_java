@SuppressWarnings("unused")

public class mySuperDoublyLinkedList {

	static {
		
	}
	
	private class Node {
		
		private int value;
		private Node next;	
		private Node prev;
		
		public Node(int newValue) {
			this.value = newValue;
		}
	}
	
	public Node head = null;
	public Node tail = null;
	
	public void add(int newValue) {
		
		Node tempNode = new Node(newValue);
		
		if (this.head == null && this.tail == null) {
			this.head = tempNode;
			this.tail = tempNode;
			tempNode.prev = null;
			tempNode.next = null;
		} else {
			tempNode.prev = this.tail;
			this.tail.next = tempNode;
			tempNode.next = null;
			this.tail = tempNode;
		}
		
	} 
	
	public int getHead() {
		return head.value;
	}
	
	public int getTail() {
		return tail.value;
	}
	
	public String getAllElems() {
		
		String str = "";
		String tempStr = "";
		Node tempNode = this.head;
		while (tempNode != null) {
			tempStr = Integer.toString(tempNode.value);
			str += tempStr;
			tempNode = tempNode.next;
			if (tempNode != null) {
				str += ", ";
			}
		}
		return str;
		
	}
	
	public int getNthElem(int n) {
		
		int nthValue = 0;
		int currentNode = 1;
		Node tempNode = this.head;
		
		while (tempNode != null) {
			if (currentNode == n) {
				nthValue = tempNode.value;
				break;
			} else {
				tempNode = tempNode.next;
				currentNode++;
			}
		}		
		
		if (tempNode == null) {
			nthValue = -1;
		}
		return nthValue;
	}
}
