//Made by Artur Zochniak
public class arturStack {
	
	private Node first = null;
	
	public boolean isEmpty() {
		return first == null;
	}
	
	public boolean isOnlyOneLeft() {
		
		boolean result = false;
		
		if (first != null) {
			Object tempVar = this.Pop();
			if (first == null) {
				result = true;
			}
			this.Push(tempVar);
		}
		
		return result;
	}
	
	public Object Pop() {
		
		if (isEmpty()) {
			return null;
		} else {
			Object temp = first.value;
			first = first.next;
			return temp;
		}
	}
		
	public void Push(Object o) {
		first = new Node(o, first);
	}
	
	class Node {
		
		public Node next;
		public Object value;
		
		public Node(Object value) {
			this(value, null); 
		}
		
		public Node(Object value, Node next) {
			this.next = next;
			this.value = value;
		}
	}
}