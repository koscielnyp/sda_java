import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

	static {
		/*long timeStart = System.currentTimeMillis();
		long timeEnd = System.currentTimeMillis();
		System.out.println("Czas wykonania: " + (timeEnd - timeStart));*/
	}
	
	public static void main(String[] args) {
		
		//some variables declaration
		String name = "Pawe�";
		int[] arrayWithValues = {1, 2, 4, 5, 67, 3, 4};
		int[] arrayMultiplies = {3, 5};
		ArrayList<Integer> arrayListWithValues = new ArrayList<>(Arrays.asList(1, 2, 4, 5, 67, 3, 4));
		List<Integer> listWithValues = (Arrays.asList(1, 2, 4, 5, 67, 3, 4));
		
		System.out.println(ConsoleHelper.writeDownWelcome(name));
		System.out.println("Max value: " + ConsoleHelper.getExtremeValue("max", arrayWithValues));
		System.out.println("Min value: " + ConsoleHelper.getExtremeValue("min", arrayWithValues));		
		System.out.println("Max value (Recur): " + ConsoleHelper.getMaxValueRecur(arrayListWithValues));
		System.out.println("Max value (Recur Alt): " + ConsoleHelper.getMaxValueRecurAlt(listWithValues, 0, -1));	
		System.out.println("Sum Multiplies: " + ConsoleHelper.sumMultiplies(arrayMultiplies, 10));
		System.out.println("Fibb without recurrence: " + ConsoleHelper.fibbNotRecur(20));
		System.out.println("Fibb with recurrence: " + ConsoleHelper.fibbRecur(20)); //Recurrence is so slow
		System.out.println("Fibb sum of evens (using Array): " + ConsoleHelper.fibbSumEvenSummingCorrectParameterAsArray(4000000));
		System.out.println("Fibb sum of evens (using Queue): " + ConsoleHelper.fibbSumEvenSummingCorrectParameterAsQueue(4000000));
		System.out.println("Biggest Palindrome: " + ConsoleHelper.findBiggestPalindrome(999, 999));
		System.out.println("Smallest value dividable by all: " + ConsoleHelper.findMinValDivByRange(1, 10));
		System.out.println("ExpoSum minus SumExpo: " + ConsoleHelper.expoSumExpo(100));
		System.out.println("Is 97 a prime: " + ConsoleHelper.isPrime(1));	
		System.out.println("Prime number 10001: " + ConsoleHelper.nthPrime(10001));
		
		/*===============================*/
		
		System.out.println("String repeat: " + StringRepeater.repeat("123", 3));
		
		/*===============================*/
		
		System.out.println("prodAdjacetDigits: " + StringProcessor.prodAdjacetDigits(13, "73167176531330624919225119674426574742355349194934"+
				"96983520312774506326239578318016984801869478851843"+
				"85861560789112949495459501737958331952853208805511"+
				"12540698747158523863050715693290963295227443043557"+
				"66896648950445244523161731856403098711121722383113"+
				"62229893423380308135336276614282806444486645238749"+
				"30358907296290491560440772390713810515859307960866"+
				"70172427121883998797908792274921901699720888093776"+
				"65727333001053367881220235421809751254540594752243"+
				"52584907711670556013604839586446706324415722155397"+
				"53697817977846174064955149290862569321978468622482"+
				"83972241375657056057490261407972968652414535100474"+
				"82166370484403199890008895243450658541227588666881"+
				"16427171479924442928230863465674813919123162824586"+
				"17866458359124566529476545682848912883142607690042"+
				"24219022671055626321111109370544217506941658960408"+
				"07198403850962455444362981230987879927244284909188"+
				"84580156166097919133875499200524063689912560717606"+
				"05886116467109405077541002256983155200055935729725"+
				"71636269561882670428252483600823257530420752963450"));
		
		/*===============================*/
		
		myStack superStack = new myStack();
		superStack.push(10);
		System.out.println("Last stack element = " + superStack.pop());
				
		/*===============================*/
		
		mySuperDoublyLinkedList supaList = new mySuperDoublyLinkedList();
		supaList.add(10);
		supaList.add(12);
		supaList.add(20);
		supaList.add(30);
		supaList.add(50);
		System.out.println("First elem: " + supaList.getHead());
		System.out.println("Last elem: " + supaList.getTail());
		System.out.println("All elems: " + supaList.getAllElems());
		System.out.println("nth elem: " + supaList.getNthElem(2));
		
		/*===============================*/
		
		System.out.println("result of '2 2 + 3 * 4 +': " + myRPN.RPN("2 2 + 3 * 4 +"));
		
		/*===============================*/
		
		int[] erej = {4,17,6,3,2};
		erej = myBubbleSort.sort(erej);
		System.out.println("Sorted {4 17 6 3 2}: " + myBubbleSort.typeAll(erej));
		
	}

}