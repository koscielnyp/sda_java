
public class StringRepeater {

	static {
		assert repeat("123", 3).equals("123123123");
	}
	
	public static String repeat(String str, int repeats) {
		
		String combinedStr = "";
		
		for (int i = 0; i < repeats; i++) {
			combinedStr += str;
		}
		return combinedStr;
	}
}
