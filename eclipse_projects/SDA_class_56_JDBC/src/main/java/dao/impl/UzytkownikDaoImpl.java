package dao.impl;

import dao.UzytkownikDao;
import model.Uzytkownik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import config.Database;

public class UzytkownikDaoImpl implements UzytkownikDao {

	private Database database = new Database();
	
    @Override
    public void zapisz(Uzytkownik uzytkownik) throws SQLException {
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "INSERT INTO uzytkownik (imie, nazwisko) VALUES (?, ?)";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	statement.setString(1, uzytkownik.getImie());
    	statement.setString(2, uzytkownik.getNazwisko());
    	
    	statement.executeUpdate();
    	
    	database.zamknijPolaczenie();
    	
    }

    @Override
    public Uzytkownik znajdz(int id) throws SQLException {
    	
    	Uzytkownik uzytkownik = null;
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "SELECT id, imie, nazwisko FROM uzytkownik WHERE id = ?";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	statement.setInt(1, id);
    	
    	statement.executeQuery();
    	
    	ResultSet result = statement.executeQuery();
    	
    	while (result.next()) {
    		String imie = result.getString("imie");
    		String nazwisko = result.getString("nazwisko");
    		
    		uzytkownik = new Uzytkownik(id, imie, nazwisko);
    	}
    	
    	database.zamknijPolaczenie();
    	
    	return uzytkownik;
    }

    @Override
    public List<Uzytkownik> znajdzWszystkich() throws SQLException {

    	List<Uzytkownik> listaUzytkownikow = new ArrayList<Uzytkownik>();
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "SELECT id, imie, nazwisko FROM uzytkownik";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	
    	statement.executeQuery();
    	
    	ResultSet result = statement.executeQuery();
    	
    	while (result.next()) {
    		int id = result.getInt("id");
    		String imie = result.getString("imie");
    		String nazwisko = result.getString("nazwisko");
    				
    		Uzytkownik uzytkownik = new Uzytkownik(id, imie, nazwisko);
    		
    		listaUzytkownikow.add(uzytkownik);
    	}
    	
    	database.zamknijPolaczenie();
    	
    	return listaUzytkownikow;
    	
    }

    @Override
    public void usun(int id) throws SQLException {

    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "DELETE FROM uzytkownik WHERE id = ?";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	statement.setInt(1, id);
    	
    	statement.executeUpdate();
    	
    	database.zamknijPolaczenie();
    	
    }
}
