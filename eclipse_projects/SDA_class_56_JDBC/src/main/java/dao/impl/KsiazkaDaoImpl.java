package dao.impl;

import dao.KsiazkaDao;
import model.Ksiazka;
import model.Uzytkownik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import config.Database;

public class KsiazkaDaoImpl implements KsiazkaDao {
	
	private Database database = new Database();

    @Override
    public void zapisz(Ksiazka ksiazka) throws SQLException {
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "INSERT INTO ksiazka (tytul, autor) VALUES (?, ?)";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	statement.setString(1, ksiazka.getTytul());
    	statement.setString(2, ksiazka.getAutor());
    	
    	statement.executeUpdate();
    	
    	database.zamknijPolaczenie();
    }

    @Override
    public Ksiazka znajdz(int id) throws SQLException {
    	
    	Ksiazka ksiazka = null;
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "SELECT id, tytul, autor FROM ksiazka WHERE id = ?";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	statement.setInt(1, id);
    	
    	statement.executeQuery();
    	
    	ResultSet result = statement.executeQuery();
    	
    	while (result.next()) {
    		String tytul = result.getString("tytul");
    		String autor = result.getString("autor");
    		
    		ksiazka = new Ksiazka(id, tytul, autor);
    	}
    	
    	database.zamknijPolaczenie();
    	
    	return ksiazka;
    }

    @Override
    public List<Ksiazka> znajdzWszystkie() throws SQLException {

    	List<Ksiazka> listaKsiazek = new ArrayList<Ksiazka>();
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "SELECT id, tytul, autor FROM ksiazka";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	
    	statement.executeQuery();
    	
    	ResultSet result = statement.executeQuery();
    	
    	while (result.next()) {
    		int id = result.getInt("id");
    		String tytul = result.getString("tytul");
    		String autor = result.getString("autor");
    				
    		Ksiazka ksiazka = new Ksiazka(id, tytul, autor);
    		
    		listaKsiazek.add(ksiazka);
    	}
    	
    	database.zamknijPolaczenie();
    	
    	return listaKsiazek;
    	
    }

    @Override
    public void usun(int id) throws SQLException {
    	
    	Connection connection = database.otworzPolaczenie();
    	
    	String sql = "DELETE FROM ksiazka WHERE id = ?";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	statement.setInt(1, id);
    	
    	statement.executeUpdate();
    	
    	database.zamknijPolaczenie();
    }
}
