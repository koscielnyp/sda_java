import sun.rmi.runtime.Log;

import java.io.FileNotFoundException;

/**
 * Created by PawelK on 24.10.2015.
 */
import java.util.Map;

public class RPSTurbo_Intro {
    public static void main(String[] Args) {

        System.out.println("RPS Turbo v1.5\nby Pawel Koscielny\n");

        String UserAnswer = "K";
        Map ShapeListActual;
        RPSTurbo_Logic Logic = new RPSTurbo_Logic();

        try {
            ShapeListActual = Logic.AnalyzeExtractedLines(RPSTurbo_Input.ReadFile()); //Wczytanie pliku i zrobienie z niego mapy gestow

            while (!UserAnswer.equals("E")) {
                Logic.AskForInput(ShapeListActual);
                UserAnswer = RPSTurbo_Input.Input();
                Logic.Play(ShapeListActual, UserAnswer);
            }

            System.out.println("\nTo czesc!");

        } catch (FileNotFoundException e) {
            System.out.println("\nPlik wymagany do gry nie istnieje!\nTo czesc!");
        }
    }
}
