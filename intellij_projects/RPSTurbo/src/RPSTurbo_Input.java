/**
 * Created by PawelK on 24.10.2015.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.nio.file.Path;
import java.nio.file.Paths;


public class RPSTurbo_Input {

    public static ArrayList ReadFile() throws FileNotFoundException {

            ArrayList LinesExtracted = new ArrayList();

            Path currentRelativePath = Paths.get("");
            String sPath = currentRelativePath.toAbsolutePath().toString();

            File FileToRead = new File(sPath + "/game_args/RPS_9");
            Scanner FileReader = new Scanner(FileToRead);

            /*int LineNumber = 0;
            while (LineCounter.hasNext()) {
                LineNumber++;
                LineCounter.nextLine();
            }

            for (int ReadCount = 0; ReadCount < LineNumber; ReadCount++) {
                LinesExtracted.add(FileReader.nextLine());
            }*/

            while (FileReader.hasNext())
                LinesExtracted.add(FileReader.nextLine());

            return LinesExtracted;
    }

    public static String Input() {

        //Odczyt klawiszy
        Scanner KeyPressReader = new Scanner(System.in);
        String UserAnswer = KeyPressReader.next();
        return UserAnswer.toUpperCase();
    }

}
