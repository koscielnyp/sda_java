import java.util.Random;

/**
 * Created by PawelK on 27.10.2015.
 *
 *
 *    NOT USED ANYMORE!!!
 *
 *
 */
public class RPSTurbo_LogicOld {

    private String[] AnswerList = new String[6];
    private Random Randomizer = new Random();

    //Konstruktor
    public RPSTurbo_LogicOld() {
        AnswerList[0] = "R";
        AnswerList[1] = "P";
        AnswerList[2] = "SC";
        AnswerList[3] = "L";
        AnswerList[4] = "SP";
        AnswerList[5] = "E";
    }

    private static void AskForInputOld() {
        System.out.println("\nWybierz: [R]ock; [P]aper; [SC]issors; [L]izard; [SP]ock; lub [E]xit by wyjsc.");
    }

    public void Play(String UserAnswer){
        if (UserAnswer.equals("R") || UserAnswer.equals("P") || UserAnswer.equals("SC") || UserAnswer.equals("L") || UserAnswer.equals("SP")) {
            int randomized_number = Randomizer.nextInt(5);
            System.out.println("Ty wybrales: " + UserAnswer);
            System.out.println("Komputer wybral: " + AnswerList[randomized_number]);

            //Wygrane

            if (UserAnswer.equals("R") && AnswerList[randomized_number].equals("L")) {
                System.out.println("Rock crushes Lizard");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("R") && AnswerList[randomized_number].equals("SC")) {
                System.out.println("Rock crushes Scissors");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("P") && AnswerList[randomized_number].equals("R")) {
                System.out.println("Paper covers Rock");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("P") && AnswerList[randomized_number].equals("SP")) {
                System.out.println("Paper disproves Spock");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("SC") && AnswerList[randomized_number].equals("P")) {
                System.out.println("Scissors cuts Paper");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("SC") && AnswerList[randomized_number].equals("L")) {
                System.out.println("Scissors decapitates Lizard");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("L") && AnswerList[randomized_number].equals("SP")) {
                System.out.println("Lizard poisons Spock");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("L") && AnswerList[randomized_number].equals("P")) {
                System.out.println("Lizard eats Paper");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("SP") && AnswerList[randomized_number].equals("SC")) {
                System.out.println("Spock smashes Scissors");
                System.out.println(">>>Wygrales!");
            } else if (UserAnswer.equals("SP") && AnswerList[randomized_number].equals("R")) {
                System.out.println("Spock vaporizes Rock");
                System.out.println(">>>Wygrales!");
            }

            //Przegrane

            else if (UserAnswer.equals("L") && AnswerList[randomized_number].equals("R")) {
                System.out.println("Rock crushes Lizard");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("R") && AnswerList[randomized_number].equals("SC")) {
                System.out.println("Rock crushes Scissors");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("R") && AnswerList[randomized_number].equals("P")) {
                System.out.println("Paper covers Rock");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("SP") && AnswerList[randomized_number].equals("P")) {
                System.out.println("Paper disproves Spock");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("P") && AnswerList[randomized_number].equals("SC")) {
                System.out.println("Scissors cuts Paper");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("L") && AnswerList[randomized_number].equals("SC")) {
                System.out.println("Scissors decapitates Lizard");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("SP") && AnswerList[randomized_number].equals("L")) {
                System.out.println("Lizard poisons Spock");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("P") && AnswerList[randomized_number].equals("L")) {
                System.out.println("Lizard eats Paper");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("SC") && AnswerList[randomized_number].equals("SP")) {
                System.out.println("Spock smashes Scissors");
                System.out.println(">>>Przegrales!");
            } else if (UserAnswer.equals("R") && AnswerList[randomized_number].equals("SP")) {
                System.out.println("Spock vaporizes Rock");
                System.out.println(">>>Przegrales!");
            } else {
                System.out.println(">>>Nikt nie wygral!");
            }
        } else {
            System.out.println("Niewlasciwy wybor!");
        }
    }
}
