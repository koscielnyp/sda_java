/**
 * Created by PawelK on 24.10.2015.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class RPSTurbo_Logic {

    private Random Randomizer = new Random();

    private void addShapeToList(Map ShapeList, String ShapeID, RPSTurbo_ShapeContainer Shape)
    {
        ShapeList.put( ShapeID, Shape );
    }

    private RPSTurbo_ShapeContainer getShape(Map ShapeList, String ShapeID)
    {
        return (RPSTurbo_ShapeContainer) ShapeList.get( ShapeID );
    }

    public Map AnalyzeExtractedLines(ArrayList ListOfLines) {

        Map PreparedShapeList = new HashMap();

        for (int ReadCount = 0; ReadCount < ListOfLines.size(); ReadCount++) {
            //System.out.println(ListOfShapes.get(ReadCount));

            Scanner LineAnalyzer = new Scanner(String.valueOf(ListOfLines.get(ReadCount)));
            LineAnalyzer.useDelimiter("\\t");

            RPSTurbo_ShapeContainer ShapeContainer = new RPSTurbo_ShapeContainer();
            ArrayList ItemsExtracted = new ArrayList();

            while (LineAnalyzer.hasNext()) {
                ItemsExtracted.add(LineAnalyzer.next());
            }

            ShapeContainer.FirstLetter = String.valueOf(ItemsExtracted.get(0));
            ShapeContainer.RestOfName = String.valueOf(ItemsExtracted.get(1));
            for (int ReadCountInner = 2; ReadCountInner < ItemsExtracted.size(); ReadCountInner+=2) {
                ShapeContainer.WinsWith.add(String.valueOf(ItemsExtracted.get(ReadCountInner)));
                ShapeContainer.WinsBy.add(String.valueOf(ItemsExtracted.get(ReadCountInner+1)));
            }

            addShapeToList(PreparedShapeList, "Shape" + ReadCount, ShapeContainer);

        }

        return PreparedShapeList;
    }

    public void AskForInput(Map ShapeList) {

        System.out.println("Wybierz:");

        for (int ReadCount = 0; ReadCount < ShapeList.size(); ReadCount++) {

            System.out.print("[" + getShape(ShapeList, "Shape" + ReadCount).FirstLetter + "]" + getShape(ShapeList, "Shape" + ReadCount).RestOfName + "; ");

        }

        System.out.print("lub [E]xit aby opuscic gre\n");
    }

    public void Play(Map ShapeList, String UserAnswer){
        int randomized_number = Randomizer.nextInt(ShapeList.size());
        int FoundElementNo = -1;
        boolean PlayerWins, CPUWins;

        //Sprawdzanie czy dobry wybor
        for (int ShapeListElementNo = 0; ShapeListElementNo < ShapeList.size(); ShapeListElementNo++) {
            if (UserAnswer.equals(getShape(ShapeList, "Shape" + ShapeListElementNo).FirstLetter.toUpperCase())) {
                FoundElementNo = ShapeListElementNo;
            } else if (UserAnswer.toUpperCase().equals("E")) {
                FoundElementNo = -2;
            }
        }

        //Jesli dobry to gramy a jak nie to nie
        if (FoundElementNo >= 0) {

            CPUWins = false;
            System.out.println("Ty wybrales: " + UserAnswer);
            System.out.println("Komputer wybral: " + getShape(ShapeList, "Shape" + randomized_number).FirstLetter.toUpperCase() + "\n");

            PlayerWins = CheckIfWinsWith(ShapeList, FoundElementNo, randomized_number);

            if (!PlayerWins) {

                CPUWins = CheckIfWinsWith(ShapeList, randomized_number, FoundElementNo);

            }

            if (!PlayerWins && !CPUWins) {
                System.out.println("Nikt nie wygral!\n");
            } else if (!PlayerWins) {
                System.out.println("Przegrales!\n");
            } else {
                System.out.println("Wygrales!\n");
            }

        } else if (FoundElementNo != -2){
            System.out.println("Niewlasciwy wybor!");
        }

    }

    private boolean CheckIfWinsWith(Map ShapeList, int FirstShape, int SecondShape) {

        for (int ArrayListElementNo = 0; ArrayListElementNo < getShape(ShapeList, "Shape" + FirstShape).WinsWith.size(); ArrayListElementNo++) {

            if (getShape(ShapeList, "Shape" + FirstShape).WinsWith.get(ArrayListElementNo).toString().toUpperCase().equals(getShape(ShapeList, "Shape" + SecondShape).FirstLetter.toUpperCase())) {
                System.out.println
                        (
                                getShape(ShapeList, "Shape" + FirstShape).FirstLetter +
                                        getShape(ShapeList, "Shape" + FirstShape).RestOfName +
                                        " " +
                                        getShape(ShapeList, "Shape" + FirstShape).WinsBy.get(ArrayListElementNo).toString() +
                                        " " +
                                        getShape(ShapeList, "Shape" + SecondShape).FirstLetter +
                                        getShape(ShapeList, "Shape" + SecondShape).RestOfName
                        );
                return true;
            }
        }
        return false;
    }

}
