<script
        src="https://code.jquery.com/jquery-3.0.0.min.js"
        integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="
        crossorigin="anonymous">
</script>
<script>
    function showProducts() {
        $.ajax({
            type: "GET",
            url: "/product",
            success: function(data)
            {
                $('#productList').html("");
                data.forEach(function(currentItem) {
                    $('#productList').append(currentItem.name + " " + (currentItem.price) + "<br/>");
                });
            }
        });
    }

    $('#addProduct').on('click', function (e) {
        var url = "/product"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            url: url,
            data: $("#productForm").serialize(), // serializes the form's elements.
            success: function(data)
            {
                showProducts()
            }
        });

        e.preventDefault();
    });

    showProducts();
</script>