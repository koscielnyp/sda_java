<script
        src="https://code.jquery.com/jquery-3.0.0.min.js"
        integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="
        crossorigin="anonymous">
</script>
<script>
    $('#submitButtonJS').on('click', function (e) {
        var url = "/validate"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            url: url,
            data: $("#formJS").serialize(), // serializes the form's elements.
            success: function(data)
            {
                if (data == 'true') {
                    $('#loginStatus').html("Udane");
                } else {
                    $('#loginStatus').html("Nieudane");
                }

            }
        });

        e.preventDefault();
    })
</script>