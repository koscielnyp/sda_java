<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <jsp:include page="header.jsp" />
    <body>
        Witaj! Twoje IP to: ${ip}<br/>
        Twoj jezyk: ${lang}<br/>
        <br/>
        <%--<jsp:include page="userModule.jsp" />--%>
        <jsp:include page="productModule.jsp" />
        <br/>
        <br/>
        <jsp:include page="footer.jsp" />
    </body>
</html>
