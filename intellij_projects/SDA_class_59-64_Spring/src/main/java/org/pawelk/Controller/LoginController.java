package org.pawelk.Controller;

import org.pawelk.Model.User;
import org.pawelk.Model.UserRepository;
import org.pawelk.Model.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class LoginController {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = GET)
    public String showLogin(Model model) {
        return "login";
    }

    @RequestMapping(value = "/login", method = POST)
    public String actualLogin(Model model, String login, String password) {

        if (userService.validateUser(login, password)) {
            User foundUser = userRepo.getUserByLogin(login);
            model.addAttribute("name", foundUser.getName());
            model.addAttribute("surname", foundUser.getSurname());
            return "loginResultOK";
        }
        return "loginNoUserFound";
    }

    @RequestMapping(value = "/validate", method = POST)
    @ResponseBody
    public String actualLoginThroughJavaScript(String login, String password) {

        return String.valueOf(userService.validateUser(login, password));
    }
}