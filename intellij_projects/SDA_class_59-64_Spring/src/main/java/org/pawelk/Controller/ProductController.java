package org.pawelk.Controller;

import org.pawelk.Model.Product;
import org.pawelk.Model.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepo;

    @RequestMapping(value = "/product", method = GET)
    @ResponseBody
    public List<Product> getProductList() {

        return productRepo.getProducts();
    }

    @RequestMapping(value = "/product", method = POST)
    @ResponseBody
    public List<Product> addProductList(String name, String price) {

        Product newProduct = new Product(name, price);
        productRepo.addProduct(newProduct);
        return productRepo.getProducts();
    }
}
