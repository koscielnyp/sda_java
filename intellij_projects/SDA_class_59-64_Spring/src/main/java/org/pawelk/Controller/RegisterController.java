package org.pawelk.Controller;

import org.pawelk.Model.User;
import org.pawelk.Model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class RegisterController {

    @Autowired
    private UserRepository userRepo;

    @RequestMapping(value = {"/register"}, method = GET)
    public String returnUsers(Model model) {
        return "register";
    }

    @RequestMapping(value = {"/register"}, method = POST)
    public String addUser(Model model, User user) {
        userRepo.addUser(user);
        return "registerSuccess";
    }
}
