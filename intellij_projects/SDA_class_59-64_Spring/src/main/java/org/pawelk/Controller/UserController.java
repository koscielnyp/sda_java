package org.pawelk.Controller;

import org.pawelk.Model.User;
import org.pawelk.Model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepo;

    @RequestMapping(value = {"/users"}, method = POST)
    public String addUser(Model model, User user) {
        userRepo.addUser(user);
        model.addAttribute("lista", userRepo.getUsers());
        return "index";
    }

    @RequestMapping(value = {"/users"}, method = GET)
    public String returnUsers(Model model) {
        model.addAttribute("lista", userRepo.getUsers());
        return "index";
    }
}
