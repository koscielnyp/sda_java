package org.pawelk.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

// @RestController //Wymienne z samo @RestController i wtedy bez @ResponseBody
@Controller
public class IndexController {

    @RequestMapping(value = {"/"}, method = GET)
    public String returnIndex(Model model, HttpServletRequest request) {
        return "index";
    }

}
