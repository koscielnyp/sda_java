package org.pawelk.Model;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {

    private List<Product> listaProduktow = new ArrayList<Product>();

    public void addProduct(Product product) {

        listaProduktow.add(product);
    }

    public List<Product> getProducts() {
        return listaProduktow;
    }
}
