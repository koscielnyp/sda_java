package org.pawelk.Model;

import org.pawelk.Model.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

    private List<User> listaUserow = new ArrayList<User>();

    public void addUser(User user) {

        listaUserow.add(user);
    }

    public List<User> getUsers() {
        return listaUserow;
    }

    public User getUserByLogin(String login) {

        for (User singleUser : listaUserow ) {
            if (singleUser.getLogin().equals(login)) {
                return singleUser;
            }
        }
        return null;
    }
}
