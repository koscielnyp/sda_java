$("#submitButton").on("click", function (e) {
    e.preventDefault();
    $.ajax({
        url: "/products",
        type: "POST",
        data: $("#myForm").serialize(),
        success: appendToProducts
    });
});

$(document).ready(function () {
    $.ajax({
        url: "/products",
        type: "GET",
        success: function (products) {
            products.forEach(appendToProducts)
        }
    });
});

function appendToProducts(product) {
    $("#products").append('<p><a href="/products/details/' + product.id + '">' + product.name + ', ' + product.price + '</a></p>');
}