<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <body>
    <spring:message code="register.form"/>
    <br/>

    <form action="/register" method="post">
        <label> <spring:message code="login"/>
            <input name="username">
        </label>
        <label>  <spring:message code="password"/>
            <input name="password" type="password">
        </label>

        <button type="submit"><spring:message code="register"/></button>

    </form>
    <c:forEach items="${errors}" var="error">
        <p><spring:message code="${error.code}"/></p>
    </c:forEach>
    <br/>
    <hr/>
    <br/>
    <a href="?lang=pl"><spring:message code="polish"/></a><br/>
    <a href="?lang=en"><spring:message code="english"/></a><br/>
    <br/>
    </body>
</html>