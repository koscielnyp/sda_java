package com.example.product;

import com.example.pagedResponse.PagedResponse;
import com.example.user.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.UUIDEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.*;

//GET -> /products - pobieranie listy produktow
//GET -> /products/{ID} - pobieranie konkretnego produktu
//POST -> /products - dodawanie produktu
//PUT -> /products/{ID} - edycja produktu
//DELETE -> /products/{ID} - usuwanie produktu

@Controller
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ProductValidator productValidator;

//    @RequestMapping(value = "/products", method = POST)
//    @ResponseBody
//    public Product products(Product p) {
//        p.setId(UUID.randomUUID().toString());
//        repository.addProduct(p);
//        return p;
//    }

    @RequestMapping(value = "/products", method = POST)
    @ResponseBody
    public Product addProduct(@RequestBody @Validated Product p) {
        if (StringUtils.isEmpty(p.getId())) {
            System.out.println("ID " + p.getId());
            p.setId(UUID.randomUUID().toString());
            repository.addProduct(p);
        } else {
            repository.update(p.getId(), p);
        }
        return p;
    }

    @RequestMapping(value = "/products/{id}", method = PUT)
    @ResponseBody
    public Product updateProduct(@RequestBody Product p, @PathVariable String id) {
        return repository.update(id, p);
    }

    @RequestMapping(value = "/products/{id}", method = DELETE)
    @ResponseBody
    public void deteleProduct(@PathVariable String id) {
        repository.remove(id);
    }

    @RequestMapping(value = "/products/{id}", method = GET)
    @ResponseBody
    public Product getProduct(@PathVariable String id) {
        return repository.getProductById(id).get();
    }

    @RequestMapping(value = "/products", method = GET)
    @ResponseBody
    public PagedResponse getPage(@RequestParam String page, @RequestParam String size) {

        PagedResponse<Product> pagedResponse = new PagedResponse<Product>();
        pagedResponse.setObjectList(repository.getPage(page, size));
        pagedResponse.setTotalPages(repository.getTotalPages(size));
        return pagedResponse;

    }
    //    public List<Product> getAll() {
    //        return repository.getAll();
    //    }

    @RequestMapping(value = "/products/details/{id}", method = GET)
    public String getProductById(@PathVariable String id, Model model) {
        model.addAttribute("product", repository.getProductById(id).get());
        return "details";
    }

    @InitBinder()
    public void initProductValidator(WebDataBinder binder){
        binder.addValidators(productValidator);
    }


}
