package com.example.product;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ProductRepository {

    private List<Product> database = new ArrayList<>();

    public void addProduct(Product p) {
       database.add(p);
    }

    public List<Product> getAll(){
        return database;
    }

    public Optional<Product> getProductById(String id) {
        return database.stream().filter(p -> id.equals(p.getId())).findAny();
    }

    public Optional<Product> getProductByName(String name) {
        return database.stream().filter(p -> name.equals(p.getName())).findAny();
    }

    public Product update(String id, Product p) {
        Product fetched = getProductById(id).get();
        fetched.setName(p.getName());
        fetched.setPrice(p.getPrice());
        return fetched;
    }

    public void remove(String id) {
        database.remove(getProductById(id).get());
    }

    public List<Product> getPage(String page, String limitPerPage) {

        List<Product> pagedList = database.stream().skip(Long.parseLong(page) * Long.parseLong(limitPerPage)).limit(Long.parseLong(limitPerPage)).collect(Collectors.toList());

        return pagedList;
    }

    public int getTotalPages(String limitPerPage) {

        return (int) Math.ceil((double) database.size() / Integer.parseInt(limitPerPage));
    }
}
