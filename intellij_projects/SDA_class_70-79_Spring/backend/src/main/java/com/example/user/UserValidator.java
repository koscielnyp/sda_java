package com.example.user;

import com.example.config.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class checkedClass) {
        return User.class.isAssignableFrom(checkedClass);
    }

    @Override
    public void validate(Object target, Errors errors)
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", Keys.USERNAME_EMPTY);

        User user = (User) target;

        if(!userService.userExistsByUsername(user.getUsername())) {
            errors.reject(Keys.USER_EXISTS);
        }

        if(user.getPassword() != null && user.getPassword().toString().length() <= 3) {
            errors.reject(Keys.PASSWORD_TOO_SHORT);
        }

    }
}
