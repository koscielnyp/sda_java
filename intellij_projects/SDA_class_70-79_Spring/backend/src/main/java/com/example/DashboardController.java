package com.example;

import com.example.config.ConfigService;
import com.example.config.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {

    @Autowired
    ConfigService configService;

    @RequestMapping("/dashboard")
    public String dashboard(Model model) {
        String costam = configService.get(Keys.COSTAM);
        String cosinnego = configService.get(Keys.COSINNEGO);

        model.addAttribute("costam", costam);
        model.addAttribute("cosinnego", cosinnego);

        return "dashboard";
    }

}
