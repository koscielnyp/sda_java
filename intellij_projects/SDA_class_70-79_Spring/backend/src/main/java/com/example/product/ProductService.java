package com.example.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;


    public Optional<Product> getProductById(String id) {
        return productRepository.getProductById(id);
    }

    public Optional<Product> getProductByName(String name) {
        return productRepository.getProductByName(name);
    }


}
