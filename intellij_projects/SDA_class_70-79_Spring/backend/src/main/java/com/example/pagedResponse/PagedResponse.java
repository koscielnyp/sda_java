package com.example.pagedResponse;
import com.example.product.Product;
import com.example.user.User;

import java.util.List;

public class PagedResponse<T> {

    private List<T> objectList;
    private int totalPages;

    public List<T> getObjectList() {
        return objectList;
    }

    public void setObjectList(List<T> objectList) {
        this.objectList = objectList;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
