package com.example.auth;

import com.example.user.User;
import com.example.user.UserService;
import com.example.user.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/register", method = GET)
    public String registerView(){
        return "register";
    }

    @RequestMapping(value = "/register", method = POST)
    public String register(@Validated User u, BindingResult result, Model model){ //wazna jest kolejnosc - binding results musi byc zaraz po walidowanym obiekcie
        if(result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors());
            return "register";
        }
        userService.add(u);
        return "redirect:/login";
    }

    @InitBinder()
    public void initUserValidator(WebDataBinder binder){
        binder.addValidators(userValidator);
    }
}
