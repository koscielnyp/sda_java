package com.example.product;

import com.example.config.Keys;
import com.example.user.User;
import com.example.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ProductValidator implements Validator {

    @Autowired
    private ProductService productService;

    @Override
    public boolean supports(Class checkedClass) {
        return Product.class.isAssignableFrom(checkedClass);
    }

    @Override
    public void validate(Object target, Errors errors)
    {

        Product product = (Product) target;

        if(productService.getProductByName(product.getName()).isPresent()) {
            errors.reject("Produkt istnieje");
        }

    }
}
