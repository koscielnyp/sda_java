package com.example.config;

public class Keys {
    public static final String COSTAM = "costam";
    public static final String COSINNEGO = "cosinnego";
    public static final String USERNAME_EMPTY = "username.empty";
    public static final String USER_EXISTS = "user.exists";
    public static final String PASSWORD_TOO_SHORT = "password.too.short";
}
