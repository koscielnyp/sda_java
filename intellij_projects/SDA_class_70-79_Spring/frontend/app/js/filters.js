app
    .filter('toUppercase', function() {
        return function(input) {
            input = input || '';
            return input.toUpperCase();
        }
    })