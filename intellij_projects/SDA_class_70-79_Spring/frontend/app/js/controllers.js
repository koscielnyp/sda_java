app
    .controller("MainCtrl", function () {
    this.testVar = 10;

    this.numbers = [1,2,3,3,3,4,5,5,5];

    this.add = function () {
        this.testVar = this.testVar + 1;
        console.log(this.testVar);
    }

    this.toggleVis = function () {
        this.showVar = !this.showVar;
    }
})

    .controller("ProductCtrl", function ($http, Product) {
        
        this.product = {};
        
        var ctrl = this;

        ctrl.pagePagin = 0;
        ctrl.sizePagin = 10;

        ctrl.getProductList = function (page, size) {
            ctrl.response = Product.get({page:page, size:size}, null, function () {
                ctrl.products = ctrl.response.objectList;
                ctrl.totalPages = ctrl.response.totalPages;
            });
        }

        ctrl.anyMorePages = function () {
            return ctrl.totalPages > (ctrl.pagePagin + 1)
        }

        ctrl.anyLessPages = function () {
            return ctrl.pagePagin != 0;
        }

        ctrl.nextPage = function () {
            ctrl.pagePagin = ctrl.pagePagin + 1;
            ctrl.getProductList(ctrl.pagePagin, ctrl.sizePagin);
        }

        ctrl.previousPage = function () {
            ctrl.pagePagin = ctrl.pagePagin - 1;
            ctrl.getProductList(ctrl.pagePagin, ctrl.sizePagin);
        }

        ctrl.getProductList(ctrl.pagePagin, ctrl.sizePagin);


        ctrl.textVar = 'Pokaż';



        this.toggleVis = function () {
            this.showVar = !this.showVar;
            this.textVar = this.textVar === 'Pokaż' ? "Ukryj":"Pokaż";
        };

        this.delProduct = function (id) {

            Product.remove(
                {id:id},
                null,
                function () {
                    ctrl.getProductList(ctrl.pagePagin, ctrl.sizePagin);
                }
            );
            // $http.delete("http://localhost:8081/products/" + id).then(function (response) {
            //     ctrl.getProductList();
            // });
            // this.products.splice(id, 1);
        };
        
        this.showProduct = function (id) {

            var singleProd = Product.get(
                {id:id},
                null,
                function() {
                    ctrl.toggleVis();
                    ctrl.singleProd = singleProd;
                    // ctrl.singleProdId = singleProd.id;
                    // // ctrl.singleProdName = singleProd.name;
                    // // ctrl.singleProdPrice = singleProd.price;
                },
                function () {
                    alert("Brak takiego produktu")
                }
            
            );

            // singleProd.price = 20000;
            // singleProd.$save();
            // ctrl.getProductList();

        }

        this.addProduct = function () {
            Product.save(
                null, 
                this.product, 
                function () {
                    ctrl.getProductList(ctrl.pagePagin, ctrl.sizePagin);
                },
                function () {
                    alert("product istnieje!")
                }
            )
            // $http.post("http://localhost:8081/products", this.product).then(function (response) {
            //     ctrl.getProductList();
            // });
            // this.product.date = new Date;
            // this.products.push(this.product);
            // this.product = {};
        };

        this.editProduct = function () {
            console.log(ctrl.singleProd)
            ctrl.singleProd.$save().then(function () {
                ctrl.toggleVis();
                ctrl.singleProd = null;
                ctrl.getProductList(ctrl.pagePagin, ctrl.sizePagin);
            });
        };

    })

    .controller("ColorCtrl", function ($rootScope) {
        $rootScope.isRed = false;
        $rootScope.isBlue = false;
        $rootScope.isGreen = false;
        $rootScope.isWhite = false;

        this.turnRed = function(){
            $rootScope.isRed = true;
            $rootScope.isBlue = false;
            $rootScope.isGreen = false;
            $rootScope.isWhite = false;
        }
        this.turnBlue = function(){
            $rootScope.isRed = false;
            $rootScope.isBlue = true;
            $rootScope.isGreen = false;
            $rootScope.isWhite = false;
        }
        this.turnGreen = function(){
            $rootScope.isRed = false;
            $rootScope.isBlue = false;
            $rootScope.isGreen = true;
            $rootScope.isWhite = false;
        }
        this.turnWhite = function(){
            $rootScope.isRed = false;
            $rootScope.isBlue = false;
            $rootScope.isGreen = false;
            $rootScope.isWhite = true;
        }
    });