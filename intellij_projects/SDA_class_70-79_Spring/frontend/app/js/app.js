var app = angular.module("SdaApp", ['ngResource', 'ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .otherwise('/products');

        $stateProvider
            .state(
                "app",
                {
                    abstract: true,
                    url: '',
                    templateUrl: 'app/view/tabs.html'
                }
            )
            .state(
                "app.products",
                {
                    url: '/products',
                    templateUrl: 'app/view/products.html'
                }
            )
            .state(
                "app.addProducts",
                {
                    url: '/products/add',
                    templateUrl: 'app/view/add.products.html'
                }
            );

    })

    
