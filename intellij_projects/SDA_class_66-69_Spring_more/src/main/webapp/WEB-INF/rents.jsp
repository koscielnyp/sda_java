<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>

<c:url value="/book-return" var="returnBookUrl"/>

<div class="container">

    <h1>List of books</h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Id</th>
                    <th class="text-center">Rent Date</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">Author</th>
                    <th class="text-center">First Name</th>
                    <th class="text-center">Last Name</th>
                    <th class="text-center col-md-1">Action</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${rents}" var="rent">
                    <tr>
                        <td>${rent.id}</td>
                        <td>${rent.createdDate}</td>
                        <td>${rent.book.title}</td>
                        <td>${rent.book.author}</td>
                        <td>${rent.user.firstName}</td>
                        <td>${rent.user.lastName}</td>
                        <td class="text-center">
                            <c:if test="${rent.status != 'FINISHED'}">
                                <form action="${returnBookUrl}" method="post">
                                    <input name="id" type="hidden" value="${rent.id}">
                                    <button type="submit" class="btn btn-sm btn-primary">Return</button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>


                </tbody>
            </table>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>