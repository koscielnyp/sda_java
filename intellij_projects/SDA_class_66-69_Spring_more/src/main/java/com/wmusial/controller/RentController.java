package com.wmusial.controller;

import com.wmusial.dao.BookRepository;
import com.wmusial.dao.RentRepository;
import com.wmusial.dao.UserRepository;
import com.wmusial.model.Book;
import com.wmusial.model.Rent;
import com.wmusial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class RentController {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RentRepository rentRepository;

    @RequestMapping(value = "/rents", method = RequestMethod.GET)
    public String getRentsView(Model model) {

        User user = userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());

        if (user.getRole().toString().equals("ADMIN")) {
            List<Rent> rents = rentRepository.findAllByOrderByCreatedDateDesc();
            model.addAttribute("rents", rents);
        } else if (user.getRole().toString().equals("USER")) {
            List<Rent> rents = rentRepository.findByUserIdOrderByCreatedDateDesc(user.getId());
            model.addAttribute("rents", rents);
        }

        return "rents";
    }

    @RequestMapping(value = "/book-rent", method = RequestMethod.POST)
    public String rentBook(@RequestParam Long id) {

        User user = userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        Book book = bookRepository.findOne(id);

        book.setAvailability(book.getAvailability() - 1);
        bookRepository.save(book);

        Rent newRent = new Rent(user, book);

        rentRepository.save(newRent);

        return "redirect:/rents";
    }

    @RequestMapping(value = "/book-return", method = RequestMethod.POST)
    public String returnBook(@RequestParam Long id) {

        Rent rentToReturn = rentRepository.findOne(id);
        Book bookToReturn = bookRepository.findOne(rentToReturn.getBook().getId());

        rentToReturn.setStatus(Rent.Status.FINISHED);

        bookToReturn.setAvailability(bookToReturn.getAvailability() + 1);
        bookRepository.save(bookToReturn);

        rentRepository.save(rentToReturn);

        return "redirect:/rents";
    }
}
