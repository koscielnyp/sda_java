package com.wmusial.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "book")
public class Book extends BaseEntity {

    @Column(name = "title")
    @Setter @Getter private String title;

    @Column(name = "author")
    @Setter @Getter private String author;

    @OneToMany(mappedBy = "book")
    @Setter @Getter private List<Rent> rents;

    @Column(name = "availability")
    @Setter @Getter private Integer availability;

    public Book() {}

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
    }
}
