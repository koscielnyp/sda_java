package com.wmusial.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "rent")
public class Rent extends BaseEntity {

    public enum Status { IN_PROGRESS, FINISHED }

    @Column(name = "created_date")
    @Setter @Getter private Date createdDate;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @Setter @Getter private Status status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Setter @Getter private User user;

    @ManyToOne
    @JoinColumn(name = "book_id")
    @Setter @Getter private Book book;

    public Rent() {}

    public Rent(User user, Book book) {
        this.user = user;
        this.book = book;
        this.createdDate = new Date();
        this.status = Status.IN_PROGRESS;
    }
}
