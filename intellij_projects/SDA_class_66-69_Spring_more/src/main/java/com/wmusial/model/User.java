package com.wmusial.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User extends BaseEntity {

    public enum Role { USER, ADMIN }

    @Column(name = "email")
    @Getter @Setter private String email;

    @Column(name = "first_name")
    @Getter @Setter private String firstName;

    @Column(name = "last_name")
    @Getter @Setter private String lastName;

    @Column(name = "password")
    @Getter @Setter private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    @Getter @Setter private Role role;

    public User() {
        role = Role.USER;
    }


}
