package main;

import config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        Menu menu = applicationContext.getBean(Menu.class, "menu");
        menu.wyswietlProgram();
    }
}
