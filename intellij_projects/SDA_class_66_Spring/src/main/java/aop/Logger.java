package aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Logger {

    private long tempTime;

    @Pointcut("execution(* config.Database.*(..))")
    public void logConnection() {

    }

    @Before("logConnection()")
    public void logStart() {
//        System.out.println("Start logging");
        tempTime = System.currentTimeMillis();
    }

    @After("logConnection()")
    public void logEnd() {

        System.out.println("SQL execution time: " + (System.currentTimeMillis() - tempTime) + "ms");
    }


    @Pointcut("execution(* dao.impl.*.*(..))")
    public void log() {

    }

    @Before("log()")
    public void beforeConnectAdvice() {

    }

    @After("log()")
    public void afterConnectAdvice() {

    }

    @Around("log()")
    public Object aroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        // przed wywolaniem

        Object returnValue = proceedingJoinPoint.proceed();

        // po wywolaniu

        return returnValue;
    }
}
