(function () {

	"use strict";
	
    localStorage.removeItem("loggedUser");
    var userCache = window.helpers.fetchFromDatabase("usersLocalDB", []);
    

    console.log("Imported login.js. Have a nice and productive day.");
    var registerForm = document.getElementById('super-form');

    registerForm.addEventListener('submit', function (e) {

        e.preventDefault();

        var emailElement = document.getElementById('formEmail');
        var passElement = document.getElementById('formPass');
		
        //sprobowac strategies z moje_funkcje.js
		var functionArray = [
			function() { var r = isEmailAddressCorrect(emailElement); return r; },
			function() { var r = checkIfPresent(passElement); return r; },
			function() { var r = actualLogin(emailElement, passElement); return r; },
            function() { var r = navigateTo(); return r; },	
		]
		
		var checkResult = checkIfPresent(emailElement);
		
		functionArray.forEach( function(currentFunc, index)  { 	
			if (checkResult === "") {
				checkResult = functionArray[index]();
			}
		} )
		
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;

    });

    var checkIfPresent = function (elem) {

        var response = "";

        if (elem.querySelector('input[type="text"]').value === "") {
            response = "Field \"" + elem.querySelector('input[type="text"]').textContent + "\" is empty.";
        }

        return response;

    }

    var isEmailAddressCorrect = function (email) {

        var response = "";
        if (email.querySelector('input[type="text"]').value.indexOf("@") === -1) {
            response = "Incorrect Email";
        }
        return response;
    }
	
    var actualLogin = function (email, password) {
		
		var response = "";
		 
        var user = {
            email: email.querySelector('input[type="text"]').value,
            password: password.querySelector('input[type="text"]').value
        };
				
		var userFoundAt = -1;
		
		if (userCache.length === 0) {
			response = "User not found!"
		} else {
			userCache.forEach( function(currentItem, index)  { 	
				if (currentItem.email === user.email) {
					userFoundAt = index;
				}		
			} )
			
			if ( userFoundAt === -1 ) {
                response = "User not found!"
			} else if ( user.password !== userCache[userFoundAt].password ){
				response = "Incorrect password"
			} else {
                localStorage.setItem("loggedUser", JSON.stringify(user));
            }
		}
		
		return response;
			
    }

    var navigateTo = function () {

		var link = document.getElementById("nextPage").click();
			
    }


}());