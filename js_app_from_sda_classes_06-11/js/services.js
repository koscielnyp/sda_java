var PasswordService = (function () {  //To sa jakies singletony - sprawdzic co to 
    
    "use strict";
    
    var instance = null;
    
    var Instance = (function () {
        function Instance() {}
        
        Instance.prototype.checkPasswordStrength = function (password) {
            
            var response = "";
            
            var actualPassword = password.querySelector('input[type="text"]').value;
            var actualPasswordLen = actualPassword.length;
            
            if (actualPasswordLen <= 8) {
                response = "Password too short";
            } else {
                var capitalsPresent = 0;
                var digitsPresent = 0;
                var f = actualPassword.charCodeAt(0);
                var l = actualPassword.charCodeAt(actualPasswordLen - 1)

                for (var i = 0; i < actualPasswordLen; ++i) {
                    if (actualPassword.charCodeAt(i) >= 48 && actualPassword.charCodeAt(i) <= 57) {
                        ++digitsPresent;
                    }
                    if (actualPassword.charCodeAt(i) >= 65 && actualPassword.charCodeAt(i) <= 90) {
                        ++capitalsPresent;
                    }
                }

                if (capitalsPresent <= 2) {
                    response += "Not enough capital letters. ";
                }

                if (digitsPresent <= 2) {
                    response += "Not enough digits.";
                }

                if (response === "") {
                    if ((f >= 65 && f <= 90) || (l >= 65 && l <= 90)) {
                        response += "First or last character cannot be a capital.";
                    }
                }

                if (response === "") {
                    if ((f >= 48 && f <= 57) && (l >= 48 && l <= 57)) {
                        response += "First and last characters cannot be a digit simultaneously.";
                    }
                }
            }
            return response;
        };
        
        Instance.prototype.doPasswordMatch = function (pass1, pass2) {
            
            var response = "";
            
            if (pass1.querySelector('input[type="text"]').value !== pass2.querySelector('input[type="text"]').value) {
                response = "Passwords do not match";
            }
            
            return response;
            
        };

        return Instance;
        
    })();
    
    return {
        get: function () {
            instance = instance || new Instance();
            return instance;
        }  
    };
    
})();

var EmailService = (function () {  //To sa jakies singletony - sprawdzic co to 
    
    "use strict";
    
    var instance = null;
    
    var Instance = (function () {
        function Instance() {}
        
        Instance.prototype.isEmailAddressCorrect = function (email) {
            
            var response = "";
            var emailInput = email.querySelector('input[type="text"]').value;
            var emailDomain = emailInput.slice( emailInput.search("@") + 1, emailInput.length - 1 );
            var emailName = emailInput.slice( 0, emailInput.search("@") );

            if ((emailInput.indexOf("@") === -1) || (emailInput.indexOf(" ") !== -1) || (emailDomain === "") || (emailName === ""))
            {
                response = "Incorrect Email";
            }

            return response;
            
        };
        
        return Instance;
        
    })();
    
    return {
        get: function () {
            instance = instance || new Instance();
            return instance;
        }  
    };
    
})();