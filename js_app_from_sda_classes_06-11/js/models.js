var User = (function () {
    
    "use strict";
    
    //konstruktor
    function User(email, password) {
        this.email = email;
        this.password = password;
    }
    
    var cache = window.helpers.fetchFromDatabase('usersLocalDB', []).map (function (object) {
        return new User(object.email, object.password);  
    });
    
    User.all = function () {
        return [].concat(cache);
    };
    
    User.findOne = function (key, value) {
        return cache.filter(function (user) {
            return user[key] === value;
        })[0] || null; //jezeli nic nie bedzie to zwroc null
    };    
    
    User.prototype.save = function () {
        cache.push(this);
        localStorage.setItem('usersLocalDB', JSON.stringify(cache));
    };
    
    /*User.prototype.toJSON = function () {
        return JSON.stringify(this);
    };*/
    //Nie potrzebne stringify, wystarczy samo:
    
    return User;
    
})();