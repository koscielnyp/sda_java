var Animal = (function () {
  function Animal() {}
  
  Animal.prototype.setDateOfBirth = function (dateString) {
    this.dateString = dateString;
  };
  
  Animal.prototype.getDateOfBirth = function () {
    return this.dateString;
  };
  
  return Animal;
})();

//var Human = (function () {
//  function User(a, b) {
//    Animal.apply(this, arguments);
//    this.name = a;
//    this.surname = b;
//  }
//  User.prototype = Object.create(Animal.prototype);
//  
//  User.prototype.fullName = function () {
//    return this.name + ' ' + this.surname;
//  };
//
//    
//  return User;
//})();

class Human extends Animal { // ES6 ONLY
  constructor() {
    super();
    this.name = 'jan';
    this.surname= 'w';
  }
  
}



//
//var user = new Human('Mateusz', 'Barczak');
//console.log(user.fullName());
//console.log(user.getDateOfBirth());
//console.log(user.setDateOfBirth('1986/09/19'));
//console.log(user.getDateOfBirth());
//user.surname = 'Kowalski';
//console.log(user.fullName());
//
//var anotherUser = new Human('Jan', 'W');
//
//console.log(anotherUser.fullName());
//
//
//

var Car = (function () {
  function Car(engine) {
    this.engine = engine;
  }
  return Car;
})();

var Engine = (function () {
  function Engine(){
    
  }
  return Engine;
})();

var engine = new Engine();

var car = new Car(new Engine());



