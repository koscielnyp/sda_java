var czyPalindrom = function(a) {
    
    var len = a.length;
    var i = 0;
    var result = "";

    for (i = 0; i < len; i++) {
        
        console.log (a[i], "->", a[(len - 1) - i]);
        
        if ( a[i] !== a[(len - 1) - i] ) {  
            result = "nie jest";
            return result;
        } else {
            result = "jest"; 
        }     
    }   
    return result;
}

function isPalindrom(str) {
    var str = str.toLowerCase();
    console.log(str);
    console.log(str.split(''));
    console.log(str.split('').reverse());
    console.log(str.split('').reverse().join(''));   
    return (str === str.split('').reverse().join(''));
}

// 'Ala ma kota kot ma ale ale nie ma kota ali'

function wordCount() { //Trzeba to zrozumiec :/
    
    var sent = "Ala ma kota kot ma ale ale nie ma kota ali";
    
    return sent.toLowerCase().split(" ").sort().reduce( function (wordCounter, word) {
        //wordCounter[word] = !wordCounter[word] ? 0 : wordCounter[word];
        wordCounter[word] = wordCounter[word] || 0;
        ++wordCounter[word];
        return wordCounter;
    }, {} );
    
}

(function () {

    console.log("SkyNET v. 1.0");

    class Animal {
        constructor() {
            this.knowsHowToSwim = false; 
        }
        
        setDateOfBirth(data) {
            this.DateOfBirth = data; 
        }
 
        getDateOfBirth() {
            return this.DateOfBirth; 
        }
        
        canSwim() {
            return this.knowsHowToSwim;
        }
    }
    
    class Person extends Animal {      
        constructor(name, surname) {
            super();
            this.name = name;
            this.surname = surname;
            this.knowsHowToSwim = true;
        }
        
        getFullName() {
            console.log(this.name + ' ' + this.surname);
        }
    }
    
    class Cow extends Animal {
        constructor() {
            super();
        }
    }
    
    var zyrafa = new Animal;
    zyrafa.setDateOfBirth("99-99-9999");
    console.log(zyrafa.getDateOfBirth());
    console.log(zyrafa.canSwim());
    
    var krowa = new Cow;
    console.log(krowa.canSwim());
    
    var ziomek = new Person("Ziomek", "Ziomalski");
    ziomek.getFullName();
    console.log(ziomek.canSwim());
  
})();
  



