(function () {
  var Engine = (function () {
    function Engine() {}

    Engine.prototype.start = function () {
      console.log('engine has started');
    };

    Engine.prototype.stop = function () {
      console.log('engine has stoped');
    };

    return Engine;
  })();


  var Car = (function () {
    function Car(engine, color) {
      this.engine = engine;
      this.color = color || 'black';
    }

    return Car;
  })();

  var car = new Car(new Engine());  
  car.engine.start();
  car.engine.stop();
  
})()
  