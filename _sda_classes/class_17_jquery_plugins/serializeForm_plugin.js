(function($) {
	
	$.fn.serializeForm = function() {
	
		var serializedObject = {};
		
		this.find("input").each( function( index, elem ) {
					
			var $elem = $(elem);
			serializedObject[$elem.attr("name")] = $elem.val();
			
		});
		
		this.find("select").each( function( index, elem ) {
					
			var $elem = $(elem);
			serializedObject[$elem.attr("name")] = $elem.val();
			
		});		
		
		return serializedObject;
	}
	
})(jQuery)