(function ($) {
	
	$.fn.rainbowText = function(text) {
		
		if (text) {			
			var textToProcess = text;
		} else {
			textToProcess = this.text();
		}
		
		if (arguments[1]) {
			$.fn.rainbowText.styles = [];
			colors = arguments[1][Object.keys(arguments[1])[0]];
			colors.forEach(function(elem) {
				$.fn.rainbowText.styles.push(elem);
			})	
		}
				
		var textArray = textToProcess.split("");
		var colorsCount = $.fn.rainbowText.styles.length;
		var currentColorIdx = 0;
		
		textArray.forEach(function(elem, index) {
			
			if (currentColorIdx === colorsCount) { currentColorIdx = 0 };
			var $tempElem = $(elem);
			var tempColor = {};
			tempColor["color"] = $.fn.rainbowText.styles[currentColorIdx];	
			++currentColorIdx;
			
			$tempElem = $("<span>", { html: elem, css: tempColor });		
			textArray[index] = $tempElem;
			
		})
		
		return this.html(textArray);
	};
	
	$.fn.rainbowText.styles = [
		'#cf000f',
		'#0f0'
	];
	
})(jQuery)