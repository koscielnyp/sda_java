-- Zajecia 41 - MYSQL

-- Warsztat #1
select * from film;
select title 'Tytuł filmu' from film limit 10;
select title, rating from film limit 100;
select distinct first_name from customer;
select distinct first_name, last_name from customer;
select distinct length from film limit 10;

-- Warsztat #2
select * from customer order by create_date desc limit 1;
select city from city order by city limit 1;
select title, length from film order by length desc;
select distinct length from film order by length;

-- Warsztat #3
select * from film where length = 168;
select * from film where length != 168;
select distinct payment_date from payment where date(payment_date) > '2005-06-15';
select * from payment where (date(payment_date) <= '2005-08-23' and amount = 4.99);
select * from address where (postal_code = 17866 or postal_code = 83579);
select * from address where postal_code in (17866, 83579);
select * from rental where (date(return_date) > '2005-05-22' and date(return_date) < '2006-01-01');
select * from rental where (date(return_date) >= '2005-05-22' and date(return_date) <= '2006-01-01');
select * from rental where return_date is null;
select * from film where description like '%Database%';
select * from film where (description like '%MySQL%' or replacement_cost = 9.99);

-- Warsztat #4
select c.first_name, c.last_name, a.address_id, a.address, a.address2 from customer c join address a on a.address_id = c.address_id;
select f.title, c.name from film f join film_category fc on f.film_id = fc.film_id join category c on fc.category_id = c.category_id;
select f.title, l.name from film f left join language l on f.original_language_id = l.language_id;
select c.first_name, c.last_name, p.amount from customer c join payment p on c.customer_id = p.customer_id order by p.amount desc limit 10;
select * from rental r right join payment p on p.rental_id = r.rental_id order by r.rental_id;

-- Warsztat #5
select concat(first_name, ' ', last_name) customer from customer where active = 1;
select substring(title, 1, 8) '8 chars from title' from film;
select title, length(title) 'title length' from film;
select date(rental_date) date, year(rental_date) year, month(rental_date) month from rental;
select length/60 'length in h' from film;
select length + 15 '+ reklamy' from film;

-- Warsztat #6
select active, count(*) count from customer group by active;
select month(payment_date) month, sum(amount) from payment where year(payment_date) = '2005' group by month;
select rating, avg(length) from film group by rating;
select postal_code, count(customer_id) count from customer c join address a on c.address_id = a.address_id group by postal_code having count(customer_id) > 1;