var form = document.getElementById('form');

form.addEventListener('submit', function (e) {
    
e.preventDefault();
    
var input = document.querySelector('input[name="password"]');
var message = document.getElementById('message');
var passLen = input.value.length;
var result = "";
    
if (passLen < 8) {
    result = "Haslo za krótkie. Wymagane: 8; Jest: " + input.value.length;
} else {
    
    var capitalPresent = false;
    var digitPresent = false;
    
    for (i = 0; i < passLen; ++i) {      
        if ( input.value.charCodeAt(i) >= 48 && input.value.charCodeAt(i) <= 57 ) { 
            digitPresent = true;
        }
        if ( input.value.charCodeAt(i) >= 65 && input.value.charCodeAt(i) <= 90 ) { 
            capitalPresent = true;
        } 
    }    
    
    if (capitalPresent === false) {    
        result += "Brak duzej litery. ";     
    }
	
    if (digitPresent === false) {    
        result += "Brak cyfry.";     
    }

}
    
if (result === "") {
    message.textContent = input.value;
} else {
    message.textContent = result;
}
  
}, false);
