class Pole {
    constructor(x,y) {
        this.x = x;
        this.y = y;
        this.players = {};
    }
    
    isOccupied(){
        return Object.keys(this.players).length > 0;
    }
    
    attach(p){
        if(this.isOccupied()){
            //console.log("there are other players on field x: "+this.x+" y: "+this.y);
            var self = this;
            $(document).trigger(main.events.COLLISION, {field: self, player: p});
        }
        this.players[p.id] = p;
    }
    
    dettach(p){
        delete this.players[p.id];
    }
}