var main = main || {};
main.events = {
    COLLISION: "collision"
};

class FightController{
    constructor(){
        var self = this;
        $(document).on(main.events.COLLISION, function(e, eventInfo){self.fight(eventInfo);});
    }
    
    fight(data){
        //console.log(data);
        var player = data.player;
        var field = data.field;
        console.log("New player on field x: " +field.x+ " y: "+ field.y+ " is "+player.id);
        for(var k in field.players){
            var p = field.players[k];
            if(p.id !== player.id){
               console.log("Player "+p.id+" was already there!");
            }
        }
    }
}