class Plansza{
    constructor(x,y){
        this.pola = {};
        for(var i=1; i<=x; i++){
            for(var j=1; j<=y; j++){
                this.pola["x"+i+"y"+j] = new Pole(i,j);
            }
        }
    }
    getField(x,y){
        return this.pola["x"+x+"y"+y];
    }
}