var main = main || {};
main.playerList = main.playerList || {};
main.playerCount = main.playerCount || 0;

class Man {

	constructor() {
		this.id = main.playerCount++;
		this.health = 100;
		this.mobility = 1;
		this.movesLeft = 1;
		this.damage = 10;
        this.time = 100;
		this.face = "gfx/arnold.png";
		main.playerList[this.id] = this;
	}
	
	spawn(x, y) {
		var playField = document.getElementById("playField");		
		var cell = playField.querySelector(".playCell[data-x = '"+ x +"'][data-y = '" + y +"']");
		var img = document.createElement("img");
		img.setAttribute("src", this.face);
		img.setAttribute("width", "20");
		img.setAttribute("height", "20");
		cell.appendChild(img);	
        this.field = main.playfield.getField(x,y);
        this.field.attach(this);
	}
	
	move() {
		
		var direction = function(){
            var x = Math.ceil(Math.random() * 10);
            while(x > 8){
                x = direction();
            }
            return x;
        }
		var posX = this.field.x;
        var posY = this.field.y;
        var playField = document.getElementById("playField");		
		var cell = playField.querySelector(".playCell[data-x = '"+ posX +"'][data-y = '" + posY +"']");
        cell.innerHTML="";
        
        switch(direction()){
            case 1: //do gory
                posY--;
                break;
            case 2: //do gory prawo
                posY--;
                posX++;
                break;
            case 3: //do prawo
                posX++;
                break;
            case 4: //do dolu prawo
                posX++;
                posY++;
                break;
            case 5: //do dolu
                posY++;
                break;
            case 6: //do dolu lewo
                posX--;
                posY++;
                break;
            case 7: //do lewo
                posX--;
                break;
            case 8: //do gory lewo
                posX--;
                posY--;
                break; 
        }
		
		if (posX> main_config.sizeX) {
			posX = 1;
		}
		if (posX < 1) {
			posX = main_config.sizeX;
		}
		if (posY > main_config.sizeY) {
			posY = 1;
		}
		if (posY < 1) {
			posY = main_config.sizeY;
		}
        this.field.dettach(this);
        this.spawn(posX, posY);
	}
	
	
	moving() {
		var self = this;
		setInterval( function(){self.move();}, self.time );
	}

}

