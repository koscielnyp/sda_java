class Hulk extends Man {

	constructor() {
        super();
		this.mobility = 1;
		this.damage = 100;
		this.face = "gfx/hulk.png";
        this.time = 500;
	}
}
