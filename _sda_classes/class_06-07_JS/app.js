(function () {
    
    window.cache = {};
    
    console.log("Imported app.js. Have a nice and productive day.");
    var registerForm = document.getElementById('super-form');

    registerForm.addEventListener('submit', function (e) {
    
        e.preventDefault();
        
        var emailElement = document.getElementById('formEmail');
        var passElement = document.getElementById('formPass');
        var passConfirmlElement = document.getElementById('formPassConfirm');
        
        var checkResult = checkIfPresent(emailElement);
        if (checkResult === "") {
            checkResult = isEmailAddressCorrect(emailElement);    
        }
        if (checkResult === "") {
            checkResult = checkIfPresent(passElement);    
        }
        if (checkResult === "") {
            checkResult = checkIfPresent(passConfirmlElement);    
        }
        if (checkResult === "") {
            checkResult = doPasswordMatch(passElement, passConfirmlElement);    
        }       
        if (checkResult === "") {
            
            //tworzenie obuiektu usera
            var save = function (email, password) {
                
                var user = {
                    email: email,
                    password: password      
                };
                cache[user.email] = user;
                showUser(user);
            }
            
            var showUser = function (user) {
                
                var showUser = function (user) {
                    
                    var userlist = document.getElementById('userList');
                    var li = document.createElement('li');
                    li.textContent = user.email;
                    userList.appendChild(li);
                }
                
            }
            
            
            checkResult = "Wszystko OK!"  
        }
        
        //alert(checkResult);
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;
        
    });
        
    var checkIfPresent = function(elem) {
    
        var response = "";
        
        if (elem.getElementsByTagName('input')[0].value === "") {
            response = "Pole \"" + elem.getElementsByTagName('label')[0].textContent + "\" jest puste.";   
        }
        
        return response;
        
    }
    
    var doPasswordMatch = function(pass1, pass2) {
        
        var response = "";
        if ( pass1.getElementsByTagName('input')[0].value !== pass2.getElementsByTagName('input')[0].value) {
            response = "Passwords do not match";
        }
        return response;
    }
    
    var isEmailAddressCorrect = function(email) {
        
        var response = "";
        if ( email.getElementsByTagName('input')[0].value.indexOf("@") === -1 ) {
            response = "Incorrect Email";
        }
        return response;       
    }

       
}());

    var updateTime = function() {
        
         var clock = document.getElementById('clock');
        clock.textContent = new Date().toLocaleTimeString();
         
    }
