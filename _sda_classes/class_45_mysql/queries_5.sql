-- Zajecia 45 - MYSQL

-- Warsztat #2
grant all on *.* to 'admin'@'localhost';
grant select on sda.* to 'moderator'@'localhost';
grant all on sda.ticket to 'moderator'@'localhost';
grant select (user_id, username, name, surname, email, birth_date) on sda.user to 'reporting'@'%';
grant select on sda.ticket to 'reporting'@'%';
revoke all on *.* from 'moderator'@'localhost';
grant select (user_id, username, name, surname, email, birth_date) on sda.user to 'moderator'@'localhost';
grant all on sda.ticket to 'moderator'@'localhost';