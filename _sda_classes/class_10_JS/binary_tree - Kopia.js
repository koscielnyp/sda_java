var init = function () {
  var canavas = document.getElementById('canvas');
  var ctx = canavas.getContext('2d');
  ctx.textAlign = 'center';
  ctx.textBaseline = 'middle';
  var root = null;

  var randomArray = function (length) {
    var arr = [];
    for(var i = 0; i < length; ++i) {
      arr.push(Math.floor(Math.random() * 100));
    }
    return arr;
  };
  
  var addNode = function (value) {
    if (!root) {
      root = createNewNode(value);
      return ;
    }
    var node = createNewNode(value);
    appendToTree(root, node, pickBranch());
  };
  
  var appendToTree = function (parent, node, side) {
    if (null == parent[side]) {
      parent[side] = node;
    } else {
      var newParent;
      while (!newParent) {
        newParent = parent[pickBranch()];
      }
      appendToTree(newParent, node, pickBranch());
    }
  };
  
  var pickBranch = function () {
    return Math.random() > 0.5 ? 'left' : 'right'
  };
  
  var createNewNode = function (value) {
    return {
      value: value,
      x: null,
      y: null,
      left: null,
      right: null
    }
  };
  
  var printTree = function (node) {
    console.log(node.value);
    if (node.left) {
      printTree(node.left);
    }
    if (node.right) {
      printTree(node.right);
    }
  };
  
  var getHeight = function (node) {
    //return 1 + Math.max.apply(null, [node.left ? getHeight(node.left) : 0,
    //                                node.right ? getHeight(node.right) : 0]);
    var leftDepth = 0;
    var rightDepth = 0;
    if (node.left) {
      leftDepth = getHeight(node.left);
    }
    if (node.right) {
      rightDepth = getHeight(node.right);
    }
    return 1 + Math.max(leftDepth, rightDepth);
  };
  
  var nodeValues = randomArray(10);
  console.log(nodeValues);
  for (var i = 0; i < nodeValues.length; ++i) {
    addNode(nodeValues[i]);
  }
  //printTree(root);
  console.log(getHeight(root));
  
  var options = {};
  var show = function () {
    setYCoords(root, 0)
    setXCoords(root);
    var maxNodes = Math.pow(2, height);
    var height = getHeight(root);
    options = {
      numOfRows: height,
      levelHeight: canvas.height/height,
      numOfColumns: maxNodes,
      columnWidth: canavas.width/maxNodes
    }
    ctx.clearRect(0, 0, canavas.width, canavas.height);
    draw(root);
  }

  var draw = function (root) {
    var q = [];
    var currNode = root;
    while (currNode) {
      var coords = getCoords(currNode);
      ctx.beginPath();
      ctx.arc(coords.x, coords.y, 20, 0, 2*Math.PI);
      ctx.font = '16px sans-serif';
      ctx.fillText(currNode.value, coords.x, coords.y);
      ctx.stroke();
      ctx.closePath();
      if (currNode !== root) {
        ctx.beginPath();
        ctx.moveTo(coords.x, coords.y);
        var parent = getParentCoords(currNode, coords);
        ctx.lineTo(parent.x, parent.y);
        ctx.stroke();
        ctx.closePath();
      }
      q = q.concat([currNode.left, currNode.right]).filter(Boolean);
      currNode = q.shift();
    }
  };

  var getCoords = function (node) {
    var colWidth = canvas.width/Math.pow(2, node.y);
    var xOffset = colWidth*(node.x + 1) - colWidth/2;
    var levelHeight = options.levelHeight;
    var yOffset = levelHeight*(node.y + 1) - (levelHeight/2);
    return {
      x: xOffset,
      y: yOffset
    };
  };

  var getParentCoords = function (node, coords) {
    var y = coords.y - options.levelHeight;
    var colWidth = canvas.width/Math.pow(2, node.y);
    var x = coords.x + Math.pow(-1, node.x)*colWidth/2;
    return {
      y: y,
      x: x
    };
  };

  show();
};

init();
