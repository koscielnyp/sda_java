var updateTime = function () {

    var clock = document.getElementById('clock');
    var showCurrentTime = function () {
        clock.textContent = new Date().toLocaleTimeString();
        setTimeout(function () {
            showCurrentTime();
        }, 1000);

    };
    showCurrentTime();
};

updateTime();