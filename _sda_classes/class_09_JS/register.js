(function () {

	"use strict";
	
    var usersLocalDB = localStorage.getItem("usersLocalDB");
    
    if (usersLocalDB) {
        window.userCache = JSON.parse(usersLocalDB);
    } else { 
        window.userCache = [];
    }
    

    console.log("Imported register.js. Have a nice and productive day.");
    var registerForm = document.getElementById('super-form');

    registerForm.addEventListener('submit', function (e) {

        e.preventDefault();

        var emailElement = document.getElementById('formEmail');
        var passElement = document.getElementById('formPass');
        var passConfirmlElement = document.getElementById('formPassConfirm');
		
        //sprobowac strategies z moje_funkcje.js
		var functionArray = [
			function() { var r = isEmailAddressCorrect(emailElement); return r; },
			function() { var r = checkIfPresent(passElement); return r; },
			function() { var r = checkPasswordStrengh(passElement); return r; },
			function() { var r = checkIfPresent(passConfirmlElement); return r; },
			function() { var r = doPasswordMatch(passElement, passConfirmlElement); return r; },
			function() { var r = checkAndAddUser(emailElement, passElement); return r; },
            function() { var r = navigateTo(); return r; },	
		]
		
		var checkResult = checkIfPresent(emailElement);
		
		functionArray.forEach( function(currentFunc, index)  { 	
			if (checkResult === "") {
				checkResult = functionArray[index]();
			}
		} )
		
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;

    });

    var checkIfPresent = function (elem) {

        var response = "";

        if (elem.querySelector('input[type="text"]').value === "") {
            response = "Field \"" + elem.querySelector('label').textContent + "\" is empty.";
        }

        return response;

    }

    var doPasswordMatch = function (pass1, pass2) {

        var response = "";
        if (pass1.querySelector('input[type="text"]').value !== pass2.querySelector('input[type="text"]').value) {
            response = "Passwords do not match";
        }
        return response;
    }

	    var isEmailAddressCorrect = function (email) {

        var response = "";
		var emailInput = email.querySelector('input[type="text"]').value;
		var emailDomain = emailInput.slice( emailInput.search("@") + 1, emailInput.length - 1 );
		var emailName = emailInput.slice( 0, emailInput.search("@") );
		
        if ((emailInput.indexOf("@") === -1) || (emailInput.indexOf(" ") !== -1) || (emailDomain === "") || (emailName === ""))
		{
            response = "Incorrect Email";
        }
	
        return response;
    }
	
    var checkPasswordStrengh = function (password) {

        var response = "";

        var actualPassword = password.querySelector('input[type="text"]').value;
        var actualPasswordLen = actualPassword.length;

        if (actualPassword.length <= 8) {
            response = "Password too short";
        } else {

            var capitalsPresent = 0;
            var digitsPresent = 0;
            var f = actualPassword.charCodeAt(0);
            var l = actualPassword.charCodeAt(actualPasswordLen - 1)

            for (var i = 0; i < actualPasswordLen; ++i) {
                if (actualPassword.charCodeAt(i) >= 48 && actualPassword.charCodeAt(i) <= 57) {
                    ++digitsPresent;
                }
                if (actualPassword.charCodeAt(i) >= 65 && actualPassword.charCodeAt(i) <= 90) {
                    ++capitalsPresent;
                }
            }

            if (capitalsPresent <= 2) {
                response += "Not enough capital letters. ";
            }

            if (digitsPresent <= 2) {
                response += "Not enough digits.";
            }

            if (response === "") {
                if ((f >= 65 && f <= 90) || (l >= 65 && l <= 90)) {
                    response += "First or last character cannot be a capital.";
                }
            }

            if (response === "") {
                if ((f >= 48 && f <= 57) && (l >= 48 && l <= 57)) {
                    response += "First and last characters cannot be a digit simultaneously.";
                }
            }

        }

        return response;
    }
	
    var checkAndAddUser = function (email, password) {
		
		var response = "";
		 
        var user = {
            email: email.querySelector('input[type="text"]').value,
            password: password.querySelector('input[type="text"]').value
        };
				
		var alreadyExists = false;
		
		if (userCache.length === 0) {
			userCache.push(user);
            localStorage.setItem("usersLocalDB", JSON.stringify(userCache));
		} else {
			userCache.forEach( function(currentItem)  { 	
				if (currentItem.email === user.email) {
					alreadyExists = true;
				}		
			} )
			
			if ( !alreadyExists ) {
				userCache.push(user);
                localStorage.setItem("usersLocalDB", JSON.stringify(userCache));
			} else {
				response = "User already exists"
			}
		}
		
		return response;
			
    }

    var navigateTo = function () {

		var link = document.getElementById("nextPage").click();
			
    }


}());