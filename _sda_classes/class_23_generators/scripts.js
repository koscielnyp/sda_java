//ciag arytmetyczny
function* ciagArytmetyczny(roznica) {
	
	var index = 1;
	
	while(true) {
		yield index;
		index += roznica;
	}
	
}

//ciag geometryczny
function* ciagGeometryczny(iloraz) {
	
	var index = 1;
	
	while(true) {
		yield index;
		index *= iloraz;
	}
	
}

//ciag fibonacciego
function* ciagFibonacciego() {
	
	position = 0;
	var array = [1,1];
	
	while(true) {
		
		yield array[position];
		++position;
		if (position === 2) {
			
			temp = array[position - 2] + array[position - 1];
			array.push(temp);  //dodajemy nowy element
			array.splice(0,1); //usuwamy pierwszy
			position -= 1; //cofamy kursor
					
		}
	}
	
}

//generator tokenow
function* rsaGen() {
	
	var phraseN = Date.now();
	var phrase = phraseN.toString();
	var previousDate = Date.now();
	
	while(true) {
		
		var generateHash = function() {
			var tempHashN = Math.abs(CryptoJS.MD5(phrase).words[0]);
			tempHash = tempHashN.toString();
			return tempHash.substring(0, 6);		
		}
		
		var newDate = Date.now();
		
		if (newDate > (previousDate + 10000)) {
			previousDate = newDate;
			phrase = generateHash();
		}
		
		hash = generateHash();
		yield hash;
	}
}

//generator losowych znakow (0-9, A-Z, a-z)
function* randomChar() {
	
	//48-57; 65-90; 97-122
	while(true) {
		var generate = function() {
			
			random = Math.floor((Math.random() * 100) + 48);
			while ( (random >= 58 && random <= 64) || (random >= 91 && random <= 96) || (random > 122) ) {
				random = generate();
			}			
			return random;
		}
		temp = String.fromCharCode(generate());
		yield temp;	
	}
}

//generator liczb pierwszych z danego zakresu
function* primes(max) {
	
	var currentValue = 2;
	
	while(currentValue <= max) {
		
		var checkForPrime = function(currentValue) {
			for (var i = 2; i < currentValue; ++i) {
				if (currentValue % i === 0) {
					return false;
					break;
				}
			}
			return true;
		}
		
		if (checkForPrime(currentValue)) {
			yield currentValue;
		}
		
		++currentValue;	
	}
}

//interator kolejnych elementow z kolekcji - tablica, drzewo, tablica wielowymiarowa, string
function* iteratorTablicowy(array) {
	
	var pos = 0;
	var endOfArray = false;
	
	while(!endOfArray) {
		
		if ( !Array.isArray(array[pos]) ) {
			yield array[pos];
		}
		
		++pos;
		
		if ( Array.isArray(array[pos]) ) {
			yield* iteratorTablicowy(array[pos]);
		}
		
		if (array[pos] == null) {
			endOfArray = true;
		}

	}
}

//generator losowych elementow z kolekcji


//przyklad skladania generatorow