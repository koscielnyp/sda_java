(function() {
	
	var link1 = document.querySelector("div[id=links]").getElementsByTagName("a")[1];	
	var blue_color = true;

	var changeColor = function () {
		if (blue_color) {
			link2.setAttribute("style", "color: blue");
			blue_color = false;
		} else {
			link2.setAttribute("style", "color: red");
			blue_color = true;
		}
	}
	
	var interval = setInterval(changeColor, 200);
	
	
	var link2 = document.querySelector("div[id=links]").getElementsByTagName("a")[2];
	var makeTextSmaller = false;
	var textSize = 12;	
	
	var changeSize = function () {
		if ( (textSize < 100) && (makeTextSmaller === false) ) {
			++textSize;
			textSizeStr = "font-size: " + textSize + "px";
			link1.setAttribute("style", textSizeStr);
		}
		
		if ( textSize === 100 ) {
			makeTextSmaller = true;
		}
		
		if ( (textSize > 2) && (makeTextSmaller === true) ) {
			textSize = textSize - 1;
			textSizeStr = "font-size: " + textSize + "px";
			link1.setAttribute("style", textSizeStr);			
		}
		
		if ( textSize === 2 ) {
			makeTextSmaller = false;
		}
	}
	
	var interval2 = setInterval(changeSize, 50);
	
	
	var multiply = function(a, b) { //sophisticated function
		return (a * b);
	}
	
	var addTabliczkaMnozenia = function() {
		
		var container = document.getElementById("tabliczka_mnozenia");
		var tabela = document.createElement("table");
		tabela.setAttribute("border", 1);
		var tbody = document.createElement("tbody");
		for (var i = 1; i <= 10; ++i) {
			var tr = document.createElement("tr");
			for (var j = 1; j <= 10; ++j) {
				var td = document.createElement("td");
				td.textContent = multiply(i, j);
				tr.appendChild(td);
			}
			tbody.appendChild(tr);
		}
		tabela.appendChild(tbody);
		container.appendChild(tabela);
		
	}
	
	addTabliczkaMnozenia();
	
} () );