var czyParzysta = function(a) { 
    var isEven = a%2 == 0;
    console.log(a, isEven);
};

var czyPalindrom = function(a) {
    
    var len = a.length;
    var i = 0;
    var result = "";

    for (i = 0; i < len; i++) {
        
        console.log (a[i], "->", a[(len - 1) - i]);
        
        if ( a[i] !== a[(len - 1) - i] ) {  
            result = "nie jest";
            return result;
        } else {
            result = "jest"; 
        }     
    }   
    return result;
}

function isPalindrom(str) {
    var str = str.toLowerCase();
    return (str === str.split('').reverse().join(''));
}

var middleChar = function(s) {
    
    var r = "";
    
    if (s.length%2 === 1) {
        
        r = s[Math.ceil(s.length/2) - 1]; 
    } else {
        
        r = (s[(s.length/2) - 1]) + (s[s.length/2]);
    }
    
    return r;
}

var fizzBuzz = function () {
    
     var r = "1";
     var rn, rt1, rt2 = "";
     
     
     for (i = 2; i <= 100; i++) {
                
         rn = ", " + i;
         rt1 = "";
         rt2 = "";
         
         if ( i%3 === 0 ) {
             
             rt1 = "Fizz";
             rn = ", " + rt1;
         }
 
         if ( i%5 === 0 ) {
             
             rt2 = "Buzz";
             rn = ", " + rt1 + rt2;
         }
         
         r = r+rn;
         
     }
    
    return r;
}


var caseTest = function(arg) {

    switch(arg) {
    
        case "Java": {

            console.log("I like Java");
            break;
        }
            
        case "Script": {

            console.log("I like Java too");
            break;
        }
            
        case "JavaScript": {

            console.log("I like Java tree");
            break;
        }
            
        default: {

            console.log("Zonk");
            break;
        }
    
    }
}

var strategiesTest = function(arg) { //poprzez zdefiniowanie obiektu z funkcjami
    
    var strategies = {
        
        Java: function () {
            console.log("I like Java");
        },

        Script: function () {
            console.log("I like Java Too");
        },

        JavaScript: function () {
            console.log("I like Java Three");
        }
    
    };
    
    if (typeof strategies[arg] === 'function') {
        strategies[arg]();
    }  else {
        console.log("Zonk");
    } 
    
}

var coDrugaLitera = function(arg) {
    
    argLen = arg.length;
    var result = "";
    
    for (var i = 0; i < argLen; ++i) {
        if ( i%2 === 0 ) {  
            result = result + arg[i];
            //mozna tez result += arg[i]
        }      
    }
    
    return result;
    
}

var coDrugaLiteraSimpler = function(arg) {
    
    argLen = arg.length;
    var result = "";
    
    for (var i = 0; i < argLen; i=i+2) {
        result += arg[i]   
    }
    
    return result;
    
}


var sumOfArray = function() {
    
    var arr = [20, 30, 40, 50, 10];
    var sum = 0;
    
    arr.forEach(
        function(currentValue)  { sum += currentValue; }
    )
    
    console.log(sum);
    
}


var  avgOfArray = function() {
    
    var arr = [20, 30, 40, 50, 10];
    var numElem = arr.length;
    var sum = 0;
    var avg = 0;
    
    arr.forEach(
        function(currentValue)  { sum += currentValue; }
    )
    
    avg = sum/numElem;
    console.log(avg);
    
}

var szukanieIgly = function() {
     
        arrSiano.forEach(
            function(currentValue, index) {
                if (currentValue === "igla") {
                    console.log("Igla znajduje sie pod: ", index);
                } 
            }
        ) 
}

var szukanieIglySimpler = function() {
     
        console.log("Igla znajduje sie pod: ", arrSiano.indexOf("igla"));
        //bez obslugi bledow to jest!
}

var Prime95 = function(arg) {
    
    var prime = 0;
    var str = "";
    
    for (i = 1; i <= arg; ++i) {
        
        prime = i;
        showValue = true;
        
        for (i2 = 2; i2 < i; ++i2) {
            
            if (i%i2 === 0) {
                showValue = false;
                break;
            }
            prime = i;
                
        }
        
        if (showValue) { str = str + i + ', '}         
    }
    
    console.log(str);
}












