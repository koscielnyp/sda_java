//Listy jednokierunkowe

class Node {

	constructor(data) {
		this.next = null;
		this.data = data;
	}		
	
}

class LinkedList {
	
	constructor() {
		this.head = null;
		this.tail = null;
		this.length = 0;
	}	
	
	addNode(data) {
		
		var newNode = new Node(data);
		
		if (this.length === 0) {
			this.head = this.tail = newNode;
		} else {
			this.tail.next = newNode;
			this.tail = newNode;
		}
		
		this.length++;
	}
	
	removeNode(data) {
		
		//to jesio nie dziala
		var currentNode = this.head;
		
		for (var i = 0; i <= this.length; ++i) {
			if (currentNode.data === data) {
				currentNode = null;
				break;
			}
		}
		
	}

}



//grafy obiektowo
class Graph {
	
	constructor() {
		this.vertices = {};
	}
	
	addVertex(vertex) {	
		if(vertex !== null && vertex.id !== null) {
			this.vertices[vertex.id] = vertex;
		}	
	}
	
	removeVertex(vertex) {
		if(vertex !== null && vertex.id !== null) {
			delete this.vertices[vertex.id];
		}		
	}
	
	connectVertices(vertex1, vertex2) {
		
		console.log("to jeszcze nie dziala, pozdrawiam");
		
	}
}


class Vertex {
	
	constructor(id) {
		this.id = id;
		this.edges = {};
	}
	
	addEdge(vertex) {
		if(vertex !== null && vertex.id !== null) {
			this.edges[vertex.id] = vertex;
		}
	}
	
	removeEdge(vertex) {
		if(vertex !== null && vertex.id !== null) {
			delete this.edges[vertex.id];
		}
	}
	
}