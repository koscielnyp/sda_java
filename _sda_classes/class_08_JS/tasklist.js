(function () {

	"use strict";
	
    var taskLocalDB = localStorage.getItem("taskLocalDB");
    var loggedUserObj = localStorage.getItem("loggedUser");
    var loggedUser = JSON.parse(loggedUserObj).email;
    
    var removeMode = true;
    
    var userInfo = document.getElementById("sub-heading").getElementsByTagName("span")[0];
    userInfo.textContent = "Zalogowany jako " + loggedUser + " ";
    
    if (taskLocalDB) {
        window.taskCache = JSON.parse(taskLocalDB);
    } else { 
        window.taskCache = [];
    }
    

    console.log("Imported tasklist.js. Have a nice and productive day.");
    console.log("Logged as: " + loggedUser);
    var registerForm = document.getElementById('super-form');
    
    registerForm.addEventListener('submit', function (e) {

        e.preventDefault();

        var taskElement = document.getElementById('formTask');
		
        //sprobowac strategies z moje_funkcje.js
		var functionArray = [
			function() { var r = addTask('test', taskElement); return r; },
            function() { var r = showTasks(); return r; },	
            //function() { var r = navigateTo(); return r; },	
		]
		
		var checkResult = checkIfPresent(taskElement);
		
		functionArray.forEach( function(currentFunc, index)  { 	
			if (checkResult === "") {
				checkResult = functionArray[index]();
			}
		} )
		
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;

    });

    var guidGen = function() {
        
        var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
        });
        
        return guid;
        
    }
    
    var checkIfPresent = function (elem) {

        var response = "";

        if (elem.getElementsByTagName('input')[0].value === "") {
            response = "Pole \"" + elem.getElementsByTagName('label')[0].textContent + "\" jest puste.";
        }

        return response;

    }
    
    var addTask = function (email, task) {
		
		var response = "";
        
        var task = {
            email: loggedUser,
            task: task.querySelector('input[name="task"]').value,
            priority: task.querySelector('input[name="priority"]').value,
            uuid: guidGen()
        };
		
        
        taskCache.push(task);
        localStorage.setItem("taskLocalDB", JSON.stringify(taskCache));
		
		return response;
			
    }
    
    var showTasks = function () {

        var taskList = document.getElementById("task-list").getElementsByTagName("ul")[0];
        while (taskList.firstChild) {
            taskList.removeChild(taskList.firstChild);
        }
        
        var taskCacheSize = taskCache.length;
        
        if (taskCacheSize !== 0) {
        
            var uniquePriorities = [];
            for ( var i = 0; i < taskCacheSize; i++) {
                if( uniquePriorities.indexOf(taskCache[i].priority) === -1 ) {
                    uniquePriorities.push(taskCache[i].priority);
                }
            }
            
            var getMaxValue = function(array) {
                var arraySize = array.length;
                var maxValue = array[0];
                for (var i = 1; i < arraySize; ++i) { 
                    if ( array[i] > maxValue ) {  
                        maxValue = array[i];
                    }
                }
                return maxValue;  
            }
 
            var getMinValue = function(array) {
                var arraySize = array.length;
                var minValue = array[0];
                for (var i = 1; i < arraySize; ++i) { 
                    if ( array[i] < minValue ) {  
                        minValue = array[i];
                    }
                }
                return minValue;  
            }
            
            var sortArray = function(array) {
                
                var arraySize = array.length;
                
                for (var i = 1; i < arraySize; ++i) {
                    for (var i2 = 1; i2 < arraySize; ++i2) {
                        if (array[i2] < array[i2 - 1]) {
                            var temp = array[i2 - 1];
                                array[i2 - 1] = array[i];
                                array[i2] = temp;
                        }
                    }
                }
                return array;
            };
            
            var invertArray = function(array) {
                
                var arraySize = array.length;
                var tempArray = [];
                
                for (var i = 0; i < arraySize; ++i) {
                    tempArray.push(array[(arraySize - 1) - i])
                }
                
                return tempArray;
            }
                
            uniquePriorities.sort(); //za latwo lol
            //var maxPriority = Math.min.apply(Math, uniquePriorities); //za latwo lol
            //var minPriority = Math.max.apply(Math, uniquePriorities); //za latwo lol
            
            //var maxPriority = getMinValue(uniquePriorities); //niepotrzebne bo mozna wziac pierwsza i ostatnia wartosc z posortowanej tablicy
            //var minPriority = getMaxValue(uniquePriorities); //niepotrzebne bo mozna wziac pierwsza i ostatnia wartosc z posortowanej tablicy    
            
            //uniquePriorities = sortArray(uniquePriorities);
            //uniquePriorities = invertArray(uniquePriorities)
            
            var maxPriority = uniquePriorities[0];
            var minPriority = uniquePriorities[uniquePriorities.length - 1];

            var renderTasksSortedByPriorities = function(priority) {

                for (var i = 0; i < taskCacheSize; ++i) {
                     if ( (taskCache[i].email === loggedUser) && (taskCache[i].priority === priority) ) {
                        var li = document.createElement("li");
                        li.textContent = taskCache[i].task;
                        if (taskCache[i].priority === maxPriority.toString()) {
                            li.setAttribute("class", "max-priority");
                        } else if (taskCache[i].priority === minPriority.toString()) {
                            li.setAttribute("class", "min-priority");
                        }
                        if ( removeMode === true ) {
                            var span = document.createElement("span");
                            span.textContent = "(X)";
                            span.setAttribute("class", "removalTool");
                            span.setAttribute("data-id", taskCache[i].uuid);
                            span.setAttribute("onclick", 'deleteMe(\"' + taskCache[i].uuid + '\")');
                            li.appendChild(span);
                        }
                        taskList.appendChild(li);
                    }               
                }

            }

            for ( var i = 0; i < uniquePriorities.length; i++) {
                renderTasksSortedByPriorities(uniquePriorities[i]);
            }
            
        }
        
    }
    
    showTasks();

    var navigateTo = function () {

		var link = document.getElementById("nextPage").click();
			
    }

    window.deleteMe = function(uuid) {
        
        var arraySize = taskCache.length;
        
        for (var i = 0; i < arraySize; ++i ) {
            
            if (taskCache[i].uuid === uuid) {
                taskCache.splice(i, 1);
                localStorage.setItem("taskLocalDB", JSON.stringify(taskCache));
                break;
            }
        } 
        showTasks();
    }

}());

