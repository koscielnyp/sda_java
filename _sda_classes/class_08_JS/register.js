(function () {

	"use strict";
	
    var usersLocalDB = localStorage.getItem("usersLocalDB");
    
    if (usersLocalDB) {
        window.userCache = JSON.parse(usersLocalDB);
    } else { 
        window.userCache = [];
    }
    

    console.log("Imported register.js. Have a nice and productive day.");
    var registerForm = document.getElementById('super-form');

    registerForm.addEventListener('submit', function (e) {

        e.preventDefault();

        var emailElement = document.getElementById('formEmail');
        var passElement = document.getElementById('formPass');
        var passConfirmlElement = document.getElementById('formPassConfirm');
		
        //sprobowac strategies z moje_funkcje.js
		var functionArray = [
			function() { var r = isEmailAddressCorrect(emailElement); return r; },
			function() { var r = checkIfPresent(passElement); return r; },
			function() { var r = checkPasswordStrengh(passElement); return r; },
			function() { var r = checkIfPresent(passConfirmlElement); return r; },
			function() { var r = doPasswordMatch(passElement, passConfirmlElement); return r; },
			function() { var r = checkAndAddUser(emailElement, passElement); return r; },
			//function() { var r = showLatestUser(); return r; },
            function() { var r = navigateTo(); return r; },	
		]
		
		var checkResult = checkIfPresent(emailElement);
		
		functionArray.forEach( function(currentFunc, index)  { 	
			if (checkResult === "") {
				checkResult = functionArray[index]();
			}
		} )
		
		
		/*
        if (checkResult === "") {
            checkResult = isEmailAddressCorrect(emailElement);
        }
        if (checkResult === "") {
            checkResult = checkIfPresent(passElement);
        }
        if (checkResult === "") {
            checkResult = checkPasswordStrengh(passElement);
        }
        if (checkResult === "") {
            checkResult = checkIfPresent(passConfirmlElement);
        }
        if (checkResult === "") {
            checkResult = doPasswordMatch(passElement, passConfirmlElement);
        }
        if (checkResult === "") {
			checkResult = checkAndAddUser(emailElement, passElement);
        }
        //if (checkResult === "") {
		//	checkResult = showLatestUser(emailElement, passElement);
        //}
		*/
		
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;

    });

    var checkIfPresent = function (elem) {

        var response = "";

        if (elem.getElementsByTagName('input')[0].value === "") {
            response = "Pole \"" + elem.getElementsByTagName('label')[0].textContent + "\" jest puste.";
        }

        return response;

    }

    var doPasswordMatch = function (pass1, pass2) {

        var response = "";
        if (pass1.getElementsByTagName('input')[0].value !== pass2.getElementsByTagName('input')[0].value) {
            response = "Passwords do not match";
        }
        return response;
    }

    var isEmailAddressCorrect = function (email) {

        var response = "";
        if (email.getElementsByTagName('input')[0].value.indexOf("@") === -1) {
            response = "Incorrect Email";
        }
        return response;
    }

    var checkPasswordStrengh = function (password) {

        var response = "";

        var actualPassword = password.getElementsByTagName('input')[0].value;
        var actualPasswordLen = actualPassword.length;

        if (actualPassword.length <= 8) {
            response = "Password too short";
        } else {

            var capitalsPresent = 0;
            var digitsPresent = 0;
            var f = actualPassword.charCodeAt(0);
            var l = actualPassword.charCodeAt(actualPasswordLen - 1)

            for (var i = 0; i < actualPasswordLen; ++i) {
                if (actualPassword.charCodeAt(i) >= 48 && actualPassword.charCodeAt(i) <= 57) {
                    ++digitsPresent;
                }
                if (actualPassword.charCodeAt(i) >= 65 && actualPassword.charCodeAt(i) <= 90) {
                    ++capitalsPresent;
                }
            }

            if (capitalsPresent <= 2) {
                response += "Not enough capital letters. ";
            }

            if (digitsPresent <= 2) {
                response += "Not enough digits.";
            }

            if (response === "") {
                if ((f >= 65 && f <= 90) || (l >= 65 && l <= 90)) {
                    response += "First or last character cannot be a capital.";
                }
            }

            if (response === "") {
                if ((f >= 48 && f <= 57) && (l >= 48 && l <= 57)) {
                    response += "First and last characters cannot be a digit simultaneously.";
                }
            }

        }

        return response;
    }
	
    var checkAndAddUser = function (email, password) {
		
		var response = "";
		 
        var user = {
            email: email.getElementsByTagName('input')[0].value,
            password: password.getElementsByTagName('input')[0].value
        };
				
		var alreadyExists = false;
		
		if (userCache.length === 0) {
			userCache.push(user);
            localStorage.setItem("usersLocalDB", JSON.stringify(userCache));
		} else {
			userCache.forEach( function(currentItem)  { 	
				if (currentItem.email === user.email) {
					alreadyExists = true;
				}		
			} )
			
			if ( !alreadyExists ) {
				userCache.push(user);
                localStorage.setItem("usersLocalDB", JSON.stringify(userCache));
			} else {
				response = "User already exists"
			}
		}
		
		return response;
			
    }
	
    var showLatestUser = function () {

		var userId = userCache.length - 1;
		var userList = document.getElementById("user-list").getElementsByTagName("ul")[0];
		var li = document.createElement('li');
		li.textContent = userCache[userId].email;
		userList.appendChild(li);
		
		return "User added!";
			
    }

    var navigateTo = function () {

		var link = document.getElementById("nextPage").click();
			
    }


}());