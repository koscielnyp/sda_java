-- Zajecia 43 - MYSQL

-- Warsztat #1
update ticket set user_id = 10 where ticket_id in (7, 8);
update user set name = 'Jan', surname = 'Kowalski' where user_id > 0;
update ticket set ticket_id = 9 where ticket_id = 8;
update ticket set user_id = (select user_id from (select u.user_id, count(t.ticket_id) from user u left join ticket t on u.user_id = t.user_id group by u.user_id order by count(t.ticket_id) limit 1) subselect1) where ticket_id = 9;

-- Warsztat #2
insert into user2 values (null, 2, 'Pawel', 'Koscielny');
delete from category where cat_id = 2;
insert into user2 values (null, 3, 'Pawel', 'Koscielny');
insert into category values (null, 'normal');
delete from user2 where cat_id = 3;

-- Warsztat #3
alter table user change surname second_name varchar(50);
alter table user add column pesel int after password;
update user set pesel = rand() * 10000 where user_id in (5, 6, 7, 8, 9);
alter table user modify pesel varchar(12);
alter table ticket drop foreign key ticket_ibfk_1;
insert into ticket values (null, 24, 'Test', 'Test2');
-- this throws error:
alter table ticket add foreign key (user_id) references user (user_id);
delete from ticket where user_id = 24;
alter table ticket add foreign key (user_id) references user (user_id);
alter table ticket drop foreign key ticket_ibfk_1;
alter table user change user_id user_id int(11);
alter table user drop primary key;
insert into user values (13, 'aaa', 'aaa', 'aaa', 'aaa', 'aaa', 'aaa@aaa.pl', date('1998-01-01'));
-- this throws error:
alter table user add primary key (user_id);
delete from user where user_id = 13 and username = 'aaa';
alter table user add primary key (user_id);
alter table user modify user_id int(11) auto_increment;
alter table ticket add foreign key (user_id) references user (user_id);