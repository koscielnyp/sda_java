(function () {

	"use strict";
	
    var taskLocalDB = localStorage.getItem("taskLocalDB");
    var loggedUserObj = localStorage.getItem("loggedUser");
    var loggedUser = JSON.parse(loggedUserObj).email;
    
    window.editMode = false;
    
    var userInfo = document.getElementById("sub-heading").getElementsByTagName("span")[0];
    userInfo.textContent = "Zalogowany jako " + loggedUser + " ";
    
    if (taskLocalDB) {
        window.taskCache = JSON.parse(taskLocalDB);
    } else { 
        window.taskCache = [];
    }
    

    console.log("Imported tasklist.js. Have a nice and productive day.");
    console.log("Logged as: " + loggedUser);
    var registerForm = document.getElementById('super-form');
	var taskList = document.getElementById("task-list").getElementsByTagName("ul")[0];
	var modeButtons = document.getElementById("modes");
    
    registerForm.addEventListener('submit', function (e) {

        e.preventDefault();

        var taskElement = document.getElementById('formTask');
		
        //sprobowac strategies z moje_funkcje.js
		var functionArray = [
			function() { var r = addTask('test', taskElement); return r; },
            function() { var r = showTasks(); return r; },	
            //function() { var r = navigateTo(); return r; },	
		]
		
		var checkResult = checkIfPresent(taskElement);
		
		functionArray.forEach( function(currentFunc, index)  { 	
			if (checkResult === "") {
				checkResult = functionArray[index]();
			}
		} )
		
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;

    });

    var deleteMe = function(uuid) {
        
        var arraySize = taskCache.length;
        
        for (var i = 0; i < arraySize; ++i ) {
            
            if (taskCache[i].uuid === uuid) {
                taskCache.splice(i, 1);
                localStorage.setItem("taskLocalDB", JSON.stringify(taskCache));
                break;
            }
        } 
        showTasks();
    }
	
    taskList.addEventListener('click', function (e) { //zrobic na STRATEGIES a na buttonach nadac DATA-ID

        e.preventDefault();
		
        var targetElement = e.target;
		
		if ( (targetElement.attributes["class"]) && (targetElement.parentNode.attributes["data-id"]) ) {
            
			if ( targetElement.attributes["class"].value === "removalTool" ) {
                var uuidToDel = targetElement.parentNode.attributes["data-id"].value;
				deleteMe(uuidToDel);
			} else if ( targetElement.attributes["class"].value === "editTool") {
                var uuidToEdit = targetElement.parentNode.attributes["data-id"].value;
				showTasks(uuidToEdit);
			} else if ( targetElement.attributes["class"].value === "saveTool" ) {
                var uuidToEdit = targetElement.parentNode.attributes["data-id"].value;
                var taskToReplace = targetElement.parentNode.querySelector('input[name="currentTaskValue"]').value;
				for (var i = 0; i < taskCache.length; ++i) {
					if ( taskCache[i].uuid === uuidToEdit ) {
						taskCache[i].task = taskToReplace;
					}
				}
				localStorage.setItem("taskLocalDB", JSON.stringify(taskCache));
				showTasks();
			} else if ( targetElement.attributes["class"].value === "tickTool" ) {
                var uuidToEdit = targetElement.parentNode.attributes["data-id"].value;
				for (var i = 0; i < taskCache.length; ++i) {
					if ( taskCache[i].uuid === uuidToEdit ) {
						taskCache[i].done = true;
					}
				}
                localStorage.setItem("taskLocalDB", JSON.stringify(taskCache));
				showTasks();
            }
		}

		
    });
	
    modeButtons.addEventListener('click', function (e) {

        e.preventDefault();
		
        var targetElement = e.target;
		if (targetElement.attributes["id"].value === "edit-mode-button") {
			window.editMode = !window.editMode;
			showTasks();
		}
		
    });

    var guidGen = function() {
        
        var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, 
			function(c) {
				var r = Math.random() * 16 | 0; //bitwise operation
				if ( c == 'x' ) {
					var v = r
				} else {
					var v = (r & 0x3 | 0x8) //bitwise operation
				}
				return v.toString(16);
			}
		);
        
        return guid;
        
    }
    
    var checkIfPresent = function (elem) {

        var response = "";

        if (elem.getElementsByTagName('input')[0].value === "") {
            response = "Pole \"" + elem.getElementsByTagName('label')[0].textContent + "\" jest puste.";
        }

        return response;

    }
    
    var addTask = function (email, task) {
		
		var response = "";
        
        var task = {
            uuid: guidGen(),
            email: loggedUser,
            task: task.querySelector('input[name="task"]').value,
            priority: task.querySelector('input[name="priority"]').value,
            done: false
        };
		
        
        taskCache.push(task);
        localStorage.setItem("taskLocalDB", JSON.stringify(taskCache));
		
		return response;
			
    }
    
    var showTasks = function (uuidToEdit) {

        while (taskList.firstChild) {
            taskList.removeChild(taskList.firstChild);
        }
        
        var taskCacheSize = taskCache.length;
        
        if (taskCacheSize !== 0) {
        
            var uniquePriorities = [];
            for ( var i = 0; i < taskCacheSize; i++) {
                if( (uniquePriorities.indexOf(taskCache[i].priority) === -1) && (taskCache[i].email === loggedUser) ) {
                    uniquePriorities.push(taskCache[i].priority);
                }
            }
            
            var getMaxValue = function(array) {
                var arraySize = array.length;
                var maxValue = array[0];
                for (var i = 1; i < arraySize; ++i) { 
                    if ( array[i] > maxValue ) {  
                        maxValue = array[i];
                    }
                }
                return maxValue;  
            }
 
            var getMinValue = function(array) {
                var arraySize = array.length;
                var minValue = array[0];
                for (var i = 1; i < arraySize; ++i) { 
                    if ( array[i] < minValue ) {  
                        minValue = array[i];
                    }
                }
                return minValue;  
            }
            
            var sortArray = function(array) {
                
                var arraySize = array.length;
                
                for (var i = 1; i < arraySize; ++i) {
                    for (var i2 = 1; i2 < arraySize; ++i2) {
                        if (array[i2] < array[i2 - 1]) {
                            var temp = array[i2 - 1];
                                array[i2 - 1] = array[i2];
                                array[i2] = temp;
                        }
                    }
                }
                return array;
            };
            
            var invertArray = function(array) {
                
                var arraySize = array.length;
                var tempArray = [];
                
                for (var i = 0; i < arraySize; ++i) {
                    tempArray.push(array[(arraySize - 1) - i])
                }
                
                return tempArray;
            }
                
            //uniquePriorities.sort(); //za latwo lol
            //var maxPriority = Math.min.apply(Math, uniquePriorities); //za latwo lol
            //var minPriority = Math.max.apply(Math, uniquePriorities); //za latwo lol
            
            //var maxPriority = getMinValue(uniquePriorities); //niepotrzebne bo mozna wziac pierwsza i ostatnia wartosc z posortowanej tablicy
            //var minPriority = getMaxValue(uniquePriorities); //niepotrzebne bo mozna wziac pierwsza i ostatnia wartosc z posortowanej tablicy    
            
			//console.log(uniquePriorities);
            uniquePriorities = sortArray(uniquePriorities);
			//console.log(uniquePriorities);
            //uniquePriorities = invertArray(uniquePriorities)
            
            var maxPriority = uniquePriorities[0];
            var minPriority = uniquePriorities[uniquePriorities.length - 1];
			//console.log(maxPriority, minPriority);
			
            var renderTasksSortedByPriorities = function(priority) {

                for (var i = 0; i < taskCacheSize; ++i) {
                     if ( (taskCache[i].email === loggedUser) && (taskCache[i].priority === priority) && (taskCache[i].done === false) ) {
						 
                        var li = document.createElement("li");
						
						if ( (uuidToEdit) && (taskCache[i].uuid === uuidToEdit) ) {
							var editCurrent = document.createElement("input");
							editCurrent.setAttribute("type", "text");
							editCurrent.setAttribute("name", "currentTaskValue");
							editCurrent.setAttribute("value", taskCache[i].task);
							li.appendChild(editCurrent);
						} else {
							li.textContent = taskCache[i].task;	
						}
						li.setAttribute("data-id", taskCache[i].uuid);
                        if (taskCache[i].priority === maxPriority.toString()) {
                            li.setAttribute("class", "max-priority");
                        } else if (taskCache[i].priority === minPriority.toString()) {
                            li.setAttribute("class", "min-priority");
                        }
                        if ( editMode === true ) {
                            var editButton = document.createElement("button");
							if ( (uuidToEdit) && (taskCache[i].uuid === uuidToEdit) ) {
								editButton.textContent = "Save";
								editButton.setAttribute("class", "saveTool");								
							} else {
								editButton.textContent = "Edit";
								editButton.setAttribute("class", "editTool");	
							}
                            li.appendChild(editButton);
                            var remButton = document.createElement("button");
                            remButton.textContent = "Del";
                            remButton.setAttribute("class", "removalTool");
                            li.appendChild(remButton);
                            var tickButton = document.createElement("button");
                            tickButton.textContent = "Done";
                            tickButton.setAttribute("class", "tickTool");
                            li.appendChild(tickButton);
                        }
                        taskList.appendChild(li);
                    }               
                }

            }

            for ( var i = 0; i < uniquePriorities.length; i++) {
                renderTasksSortedByPriorities(uniquePriorities[i]);
            }
            
        }
        
    }
    
    showTasks();

    var navigateTo = function () {

		var link = document.getElementById("nextPage").click();
			
    }

}());

