var BinaryTree = (function () {
    
    //var BinaryTree = function BinaryTree() {}  // 'var BinaryTree' nie jest potrzebne, mozna jak nizej
    function BinaryTree() {} //BinaryTree jest funkcja uzywana tylko wewnatrz zewnetrznej, wiec moze sie tak nazywac bo nie wychodzi 'na zewnatrz'
    
    BinaryTree.show = function () { //to bedzie dostepne po wywolaniu bezposrednio
        console.log('show class');
    }
    
    BinaryTree.prototype.show = function () { //to bedzie dostepne tylko w instancji
        console.log('show instance');
    }
    
    return BinaryTree; //zwraca wynik wewnetrznego! BinaryTree 
})();

var binaryTree = new BinaryTree(); //tworzymy nowa instancje BinaryTree
binaryTree.show(); //wywolujemy funkcje show NA INSTANCJI

BinaryTree.show(); //wywolujemy funkcje show bezposrednio na BinaryTree
