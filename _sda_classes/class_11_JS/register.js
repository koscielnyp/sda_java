(function () {

	"use strict";
    var userCache = window.helpers.fetchFromDatabase("usersLocalDB", []);
    

    console.log("Imported register.js. Have a nice and productive day.");
    var registerForm = document.getElementById('super-form');

    registerForm.addEventListener('submit', function (e) {

        e.preventDefault();

        var emailElement = document.getElementById('formEmail');
        var passElement = document.getElementById('formPass');
        var passConfirmlElement = document.getElementById('formPassConfirm');
		
        //sprobowac strategies z moje_funkcje.js
		var functionArray = [
			function() { var r = isEmailAddressCorrect(emailElement); return r; },
			function() { var r = checkIfPresent(passElement); return r; },
			function() { var r = checkPasswordStrengh(passElement); return r; },
			function() { var r = checkIfPresent(passConfirmlElement); return r; },
			function() { var r = doPasswordMatch(passElement, passConfirmlElement); return r; },
			function() { var r = checkAndAddUser(emailElement, passElement); return r; },
            function() { var r = navigateTo(); return r; },	
		]
		
		var checkResult = checkIfPresent(emailElement);
		
		functionArray.forEach( function(currentFunc, index)  { 	
			if (checkResult === "") {
				checkResult = functionArray[index]();
			}
		} )
		
        var finalResponse = document.getElementById('response');
        finalResponse.textContent = checkResult;

    });

    var checkIfPresent = function (elem) {

        var response = "";

        if (elem.querySelector('input[type="text"]').value === "") {
            response = "Field \"" + elem.querySelector('label').textContent + "\" is empty.";
        }

        return response;

    }

    var doPasswordMatch = function (pass1, pass2) {

        return PasswordService.get().doPasswordMatch(pass1, pass2);
    }

    var isEmailAddressCorrect = function (email) {

       return EmailService.get().isEmailAddressCorrect(email)
    }
	
    var checkPasswordStrengh = function (password) {
        
        return PasswordService.get().checkPasswordStrength(password);
    }
	
    var checkAndAddUser = function (email, password) {
		
		var response = "";
		 
        var email = email.querySelector('input[type="text"]').value;
        var password = password.querySelector('input[type="text"]').value;
		
		if (userCache.length === 0) {
            var user = new User(email, password);  //"User" object  can be found in "models.js"
            user.save();
		} else {
            if (User.findOne("email", email)) {
                response = "User already exists"
            } else {
                var user = new User(email, password);
                user.save();
            }
		}
		
		return response;
			
    }

    var navigateTo = function () {

		var link = document.getElementById("nextPage").click();
			
    }


}());