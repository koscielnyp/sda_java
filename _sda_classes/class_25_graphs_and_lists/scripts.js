var mapa = {};

var lca = { nazwa: "lca", polaczenia: [] };
var wro = { nazwa: "wro", polaczenia: [] };
var wwa = { nazwa: "wwa", polaczenia: [] };

mapa.lca = lca;
mapa.wro = wro;
mapa.wwa = wwa;

lca.polaczenia.push(wro);
wro.polaczenia.push(lca);
wro.polaczenia.push(wwa);
wwa.polaczenia.push(wro);


// usuwanie miast i polaczen
function usunMiasto(obiekt_do_usuniecia) {

	delete mapa[obiekt_do_usuniecia.nazwa]; //usuwamy samo miasto

	for (var i = 0; i < obiekt_do_usuniecia.polaczenia.length; ++i) {
		
		var miasto = obiekt_do_usuniecia.polaczenia[i];
		
		for (var j = 0; j < miasto.polaczenia.length; ++j) {
			polaczenie = miasto.polaczenia[j];
			if(polaczenie.nazwa === obiekt_do_usuniecia.nazwa) {
				delete miasto.polaczenia[j];
				break;
			}	
		}
	}
}



//druga, lepsza metoda

function connectCities(city1, city2) {
	city1.connections[city2.name] = city2;
	city2.connections[city1.name] = city1;
}

function findCity(city) {
	
	return BetterMap[city];
	
}


var BetterMap = {};

var leg = { name: "leg", connections: {} };
var cze = { name: "cze", connections: {} };
var poz = { name: "poz", connections: {} };

BetterMap.leg = leg;
BetterMap.cze = cze;
BetterMap.poz = poz;

connectCities(cze, leg);
connectCities(cze, poz);

function removeConnection(city) {
	
	var cityName = city.name;
	delete BetterMap[cityName];
	
	for (var connectedCity in city.connections){
		var innerCity = findCity(connectedCity)
		delete innerCity.connections[cityName];
	}
	
}


//Listy jednokierunkowe

var someListElem = class {
	
	constructor(data) {
		this.dane = data;
		this.next = {};
		this.prev = {};
	}
	
}

var linkedList = class {
	
	constructor() {
		this.head = {};
		this.length = 0;
	}	
	
}

function startList(data) {
	
	listElem = new someListElem(data);
	newList = new linkedList;
	newList.head = listElem;
	newList.length = 1;
	return newList;
	
}

function addToList(list, data) {
	
	//to jeszcze nie dziala
	newElem = new someListElem(data);
	list.head.next = newElem;
	list.head = newElem;
	++list.length;
	
	
}

