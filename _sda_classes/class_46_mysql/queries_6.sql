-- Zajecia 46 - MYSQL

-- Warsztat #1
-- Zadanie 1
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetPayments`(IN boundary_amount VARCHAR(50), OUT result_1 INT)
BEGIN

SELECT COUNT(*) INTO result_1
FROM payment where amount < boundary_amount;

END

call GetPayments(10, @result_1);
select @result_1;


-- Zadanie 2
-- nie dziala, bo nie sprawdza kolumny amount, zmienna amount ma pierwszenstwo
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetPayments`(IN amount VARCHAR(50), OUT result_2 INT)
BEGIN

SELECT COUNT(*) INTO result_2
FROM payment where amount < amount;

END

call GetPayments(10, @result_2);
select @result_2;

-- Zadanie 3
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetPopularMovies`(IN num INT)
BEGIN

SELECT f.title, COUNT(r.rental_id) count
from film f
join inventory i
	on f.film_id = i.film_id
join rental r
	on i.inventory_id = r.inventory_id
group by f.film_id
order by count(r.rental_id)
desc limit num;

END

call GetPopularMovies(10);

-- Zadanie 4
CREATE DEFINER=`root`@`localhost` PROCEDURE `IncrementValues`(INOUT amount INT, IN year INT, month INT)
BEGIN

IF (amount IS NULL) THEN
	SET amount = 0;
END IF;

set @temp = 0;

select count(rental_id) into @temp from rental
where
	year(rental_date) = year
    and month(rental_date) = month;
    
set amount = amount +  @temp;

END

call IncrementValues(@amount, 2005, 5);
select @amount;
call IncrementValues(@amount, 2006, 2);
select @amount;

-- Warsztat #2
-- Zadanie 1
CREATE DEFINER=`root`@`localhost` PROCEDURE `Rating`(in c_id int, out rating VARCHAR(10))
BEGIN

declare spendings decimal;

select sum(p.amount) into spendings
from payment p
	join customer c on p.customer_id = c.customer_id
where c.customer_id = 1;

if (spendings < 3) then
	set rating = 'LOW';
elseif (spendings >= 3 and spendings < 10) then
	set rating = 'NORMAL';
else
	set rating = 'VIP';
end if;

END

call Rating(534, @rat);
select @rat;

-- Zadanie 3
CREATE DEFINER=`root`@`localhost` PROCEDURE `RandomPaymentIdUnder100`(OUT concatId VARCHAR(1000))
BEGIN

DECLARE currentSum decimal default 0;

set concatId = '';

WHILE currentSum < 100 DO

	select concat(concatId, ',', payment_id), amount + currentSum into concatId, currentSum
    from payment
    order by rand()
    limit 1;
    
END WHILE;

END

call RandomPaymentIdUnder100(@ids);
select @ids;

-- Zadanie 5
CREATE DEFINER=`root`@`localhost` PROCEDURE `LoopRandomPaymentIds`(OUT concatId varchar(1000))
BEGIN

	declare totalSum decimal default 0;
    declare currAmount decimal;
    declare currId int;
    
    set concatId = '';
    
    petla: loop
    if totalSum > 100
		then leave petla;
    end if;
    
    select payment_id, amount into currId, currAmount
    from payment
    order by rand()
    limit 1;
    
    set totalSum = totalSum + currAmount;
    set concatId = concat(currId, ',', concatId);

    end loop;
	
END

call LoopRandomPaymentIds(@idsloop);
select @idsloop;