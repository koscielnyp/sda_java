-- Zajecia 44 - MYSQL

-- Warsztat #2
-- Zgloszenia tez znikaja:
delete user from user join ticket where user.user_id = ticket.user_id;
delete ticket from ticket join user where user.user_id = ticket.user_id and date(user.birth_date) > "1970-01-01";
delete user, ticket from ticket join user where user.user_id = ticket.user_id and date(user.birth_date) > "1970-01-01";

-- Warsztat #4
create view user_occupation as select u.user_id user_id, u.name name, count(t.ticket_id) num_of_tickets from user u left join ticket t on u.user_id = t.user_id group by u.user_id;
create view average_age as select avg((year(current_date()) - year(birth_date))) as avg_age from user;
create view user_ticket as select u.user_id, u.username, u.name, u.surname, u.email, u.birth_date, t.topic, t.content from user u join ticket t on u.user_id = t.user_id;
update user_ticket set topic = 'Topic 4' where user_id = 2;
-- Zmienia sie dla wielu ticketow:
update user_ticket set name = 'Adrian2' where topic = 'Topic 1';
-- Zwraca: Error Code: 1288. The target table user_occupation of the UPDATE is not updatable:
update user_occupation set name = 'Adrian2' where number_of_tickets = 2;

-- Warsztat #5
create user 'admin'@'localhost' identified by 'mocnehaslo';
create user 'moderator'@'localhost' password expire interval 30 day;
create user 'reporting'@'%' password expire interval 60 day;