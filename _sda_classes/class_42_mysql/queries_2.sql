-- Zajecia 42 - MYSQL

-- Warsztat #2
create database sda;
use sda;

-- Warsztat #3
create table user (
	user_id int auto_increment not null,
    username varchar(50) not null,
    password varchar(50) default 'adminadmin' not null,
    name varchar(50) not null,
    surname varchar(50) not null,
    email varchar(50) not null,
    birth_date date not null,
    primary key (user_id));

create table ticket (
	ticket_id int auto_increment not null,
    user_id int not null,
    topic varchar(100) not null,
    content text,
    primary key (ticket_id),
    foreign key (user_id) references user (user_id),
    index (ticket_id, user_id)
);

create table user_copy as (select * from user);

create table user_ticket as (select u.*, t.ticket_id, t.topic, t.content from user u join ticket t on u.user_id = t.user_id);

-- Warsztat #4
insert into user values
	(null, 'pawelk', md5(rand()), 'Paweł', 'Kościelny', 'a@a.pl', date('1987-01-02')), 
	(null, 'ascz', md5(rand()), 'xzvx', 'adada', 'azcz.pl', date('2000-01-02')),
	(null, 'czxc', md5(rand()), 'vxcvx', 'adad', 'adada.pl', date('1242-01-02')),
	(null, 'dsfsfs', md5(rand()), 'adada', 'vxcvx', 'a@vx.pl', date('2342-01-02'))
;

-- Throws Duplicate entry '1' for key 'PRIMARY'
insert into user values (1, 'pawelk', md5(rand()), 'Paweł', 'Kościelny', 'a@a.pl', date('1987-01-02'));

insert into ticket values
	(null, 1, 'Test', 'TestTest'),
	(null, 5, 'Test2', 'Test2Test'),
    (null, 6, 'Test3', 'Test3Test'),
    (null, 7, 'Test4', 'Test4Test')
;

-- Throws Column 'user_id' cannot be null
insert into ticket values (null, null, 'Test5', 'Test5Test');

insert into user
select 
	null as user_id, 
    substring(md5(rand()), 1, 10) as username,
    md5(rand()) as password,
    u.name, 
    u2.surname,
    concat(substring(md5(rand()), 1, 8), '@', substring(md5(rand()), 1, 3), '.pl') as email,
    date('2000-01-01') as birth_date
from user u join user u2 where (u.name != u2.name and u.surname != u2.surname);